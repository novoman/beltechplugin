<div class="grid">
    <div class="col-1-1">
        <div class="grid grid-border">
            #foreach($developer in $availableTodayDevelopers)
                #set($report = $dailyReportHelper.getReportForUser($developer))
                <div class="col-1-2">
                    <div class="module">
                        <div class="grid">
                            <div class="col-avatar">
                                <img src="$avatarService.getAvatarAbsoluteURL($currentUser, $developer, $avatarSize)" id="avatar_$developer.getId()" class="avatar" title="$developer.getDisplayName()">
                            </div>
                            <div class="col-desc">
                                <table class="user-issues">
                                    <tbody>
                                    <tr>
                                        <td class="user-name" colspan="3">
                                            <strong><a href="/secure/ViewProfile.jspa?name=$developer.getUsername()">$developer.getDisplayName()</a></strong>
                                            #if ($report && ($dailyReportHelper.getNotesFromReport($report) != "" || $dailyReportHelper.checkForCheckboxes($report)))
                                                <span class="notes-link"><a data-id="$developer.getId()">Notes</a></span>
                                                <div style="display: none;" id="notes-dialog-$developer.getId()" title="$developer.getDisplayName()'s notes">
                                                    <p><i>Submitted on $report.getCreatedAt().toString().substring(0, 10)</i></p>
                                                    <p>$dailyReportHelper.getNotesFromReport($report)</p>
                                                </div>
                                            #else
                                                <span class="daily-report has-no-report">has no report</span>
                                            #end
                                            #if ($report && $dailyReportHelper.wantsToTalk($report))
                                                <span class="daily-report wants-to-talk">wants to talk</span>
                                            #end
                                        </td>
                                    </tr>
                                        #set($issues = $jqlHelper.getActiveIssuesForUser($developer))
                                        #foreach($issue in $issues)
                                        <tr>
                                            <td>
                                                <p class="truncate" style="width: 528px;">
                                                    #if ($issue.getPriority() && $issue.getPriority().getRasterIconUrl())
                                                        <img class="icon" src="$issue.getPriority().getRasterIconUrl()"
                                                             title="$issue.getPriority().getName()"
                                                             alt="$issue.getPriority().getName()">
                                                    #end
                                                    <span class="#if($report && $dailyReportHelper.isIssueGoingToFinish($report, $issue))issue-will-be-ready#end $dailyReportHelper.isDisabledFont($showMyTeam, $issue)"
                                                            title="Issue $issue.getKey() #if($report && $dailyReportHelper.isIssueGoingToFinish($report, $issue)) will be ready today #end">$issue.getKey()</span>
                                                    <a class="$dailyReportHelper.isDisabledFont($showMyTeam, $issue)" title="$issue.getSummary()" href="/browse/$issue.getKey()">$issue.getSummary()</a>
                                                </p>
                                            </td>
                                            #set($eta = $dailyReportHelper.getETAForIssue($issue))
                                            <td class="eta shaded-font">$eta.intValue()<small>h</small></td>
                                            #if (!$showMyTeam)
                                                <td class="initials shaded-font" title="$dailyReportHelper.getBeltechPmName($issue, true)">$dailyReportHelper.getBeltechPmName($issue, false)</td>
                                            #end
                                        </tr>
                                        #end

                                        #set($sharedIssues = $jqlHelper.getActiveSharedIssuesByIds($dailyReportHelper.getSharedIssuesIdsStringForUser($developer)))
                                        #foreach($issue in $sharedIssues)
                                            #set($sharedIssue = $dailyReportHelper.getSharedIssueRecordByUserAndIssue($developer, $issue))
                                            <tr>
                                                <td>
                                                    <p class="truncate" style="width: 528px;">
                                                        #if ($issue.getPriority() && $issue.getPriority().getRasterIconUrl())
                                                            <img class="icon" src="$issue.getPriority().getRasterIconUrl()"
                                                                 title="$issue.getPriority().getName()"
                                                                 alt="$issue.getPriority().getName()">
                                                        #end
                                                        <span class="#if($report && $dailyReportHelper.isIssueGoingToFinish($report, $issue))issue-will-be-ready#end $dailyReportHelper.isDisabledFont($showMyTeam, $issue)"
                                                            title="Shared issue $issue.getKey() #if($report && $dailyReportHelper.isIssueGoingToFinish($report, $issue)) will be ready today #end">S-$issue.getKey()</span>                                                        <a title="$issue.getSummary()"
                                                        <a class="$dailyReportHelper.isDisabledFont($showMyTeam, $issue)" href="/browse/$issue.getKey()">$issue.getSummary()</a>
                                                    </p>
                                                </td>
                                                #set($eta = $dailyReportHelper.getETAForIssue($issue))
                                                <td class="eta shaded-font">$eta.intValue()<small>h</small></td>
                                                #if (!$showMyTeam)
                                                    <td class="initials shaded-font" title="$dailyReportHelper.getBeltechPmName($issue, true)">$dailyReportHelper.getBeltechPmName($issue, false)</td>
                                                #end
                                            </tr>
                                        #end

                                        #set($activityIssues = $dailyReportHelper.getOnlyIssuesWithActivityForUser($developer))
                                        #foreach($issue in $activityIssues)
                                            <tr>
                                                <td>
                                                    <p class="truncate" style="width: 528px;">
                                                        #if ($issue.getPriority() && $issue.getPriority().getRasterIconUrl())
                                                            <img class="icon" src="$issue.getPriority().getRasterIconUrl()"
                                                                 title="$issue.getPriority().getName()"
                                                                 alt="$issue.getPriority().getName()">
                                                        #end
                                                        <span class="#if($report && $dailyReportHelper.isIssueGoingToFinish($report, $issue))issue-will-be-ready#end $dailyReportHelper.isDisabledFont($showMyTeam, $issue)"
                                                              title="Issue $issue.getKey() #if($report && $dailyReportHelper.isIssueGoingToFinish($report, $issue)) will be ready today #end">$issue.getKey()</span>
                                                        <a class="$dailyReportHelper.isDisabledFont($showMyTeam, $issue)" title="$issue.getSummary()" href="/browse/$issue.getKey()">$issue.getSummary()</a>
                                                    </p>
                                                </td>
                                                #set($eta = $dailyReportHelper.getETAForIssue($issue))
                                                <td class="eta shaded-font">$eta.intValue()<small>h</small></td>
                                                #if (!$showMyTeam)
                                                    <td class="initials shaded-font" title="$dailyReportHelper.getBeltechPmName($issue, true)">$dailyReportHelper.getBeltechPmName($issue, false)</td>
                                                #end
                                            </tr>
                                        #end
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                #set($report = false)
            #end
            #if (($availableTodayDevelopers.size() > 0) && ($availableTodayDevelopers.size() % 2 == 1))
                <div class="col-1-2">
                    <div class="module"></div>
                </div>
            #end
        </div>
    </div>
    #if ($unavailableTodayDevelopers && $unavailableTodayDevelopers.size() > 0)
        #parse("/templates/pm-dashboard/unavailable-today.vm")
    #end
</div>
