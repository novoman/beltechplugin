AJS.$(function() {
    AJS.$('#start-date').datepicker({ dateFormat: "yy-mm-dd", firstDay: 1 });
    AJS.$('#end-date').datepicker({ dateFormat: "yy-mm-dd", firstDay: 1 });

    AJS.$('#start-date').blur();
});
