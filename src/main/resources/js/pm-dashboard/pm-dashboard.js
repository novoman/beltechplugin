$(function() {
    $(document).on("click", ".notes-link a", function () {
        var user_id = $(this).data("id");
        $("#notes-dialog-" + user_id).dialog({
            modal: true,
            width: 800,
            minHeight: 200,
            show: { effect: "fade", duration: 200 },
            hide: { effect: "fade", duration: 200 },
            open: function() {
                $(document).on("click", ".ui-widget-overlay", function () {
                    if ($("#notes-dialog-" + user_id).dialog('isOpen')) {
                        $("#notes-dialog-" + user_id).dialog("close");
                    }
                });
            },
            position: {
                my: "center top",
                at: "center top+250",
                of: "body"
            },
        });
    });

    $(document).on("click", "a.add-shared-issue", function () {
        var user_id = $(this).data("id");
        $("#shared-issue-dialog").dialog({
            modal: true,
            minHeight: 200,
            title: "Add shared issue",
            show: { effect: "fade", duration: 200 },
            hide: { effect: "fade", duration: 200 },
            open: function() {
                $(document).on("click", ".ui-widget-overlay", function () {
                    if ($("#shared-issue-dialog").dialog('isOpen')) {
                        $("#shared-issue-dialog").dialog("close");
                    }
                });
                $("#shared-issues-user-id").attr("multiple", "multiple");
                $("#shared-issues-user-id").attr("data-default-size", "7");
                $("#shared-issues-form").find("input[type=text], input[type=number], input[type=hidden], textarea").val("");
                $("#shared-issues-issue-key").val("PS-");
                $("#shared-issues-user-id option:selected").removeAttr("selected");
                $("#shared-issues-user-id option[value="+ user_id + "]").attr('selected', 'true');

                $('#shared-issues-start-date').datepicker({ dateFormat: "yy-mm-dd", firstDay: 1 });
                $('#shared-issues-end-date').datepicker({ dateFormat: "yy-mm-dd", firstDay: 1 });
            }
        });
    });

    $(document).on("click", "#add-shared-issue", function (e) {
        e.preventDefault();
        $.ajax({
            type: "post",
            url: "/rest/pm-dashboard-rest/1.0/pm-dashboard/addSharedIssue",
            data: $("#shared-issues-form").serialize()
        }).done(function (data, textStatus) {
            showMessage("success", data);
            if ($("#shared-issue-dialog").dialog("isOpen")) {
                $("#shared-issue-dialog").dialog("close");
            }
            $("#pm-dashboard").load(location.href + " #pm-dashboard-inner");
        }).fail(function (error) {
            showMessage("error", error.responseText);
        });
    });

    $(document).on("click", ".delete-shared-issue", function (e) {
        if (confirm("Are you sure?")) {
            $.ajax({
                type: "post",
                url: "/rest/pm-dashboard-rest/1.0/pm-dashboard/deleteSharedIssue",
                data: { 'shared-issues-id': $(this).data('id') }
            }).done(function (data, textStatus) {
                showMessage("success", data);
                $("#pm-dashboard").load(location.href + " #pm-dashboard-inner");
            }).fail(function (error) {
                showMessage("error", error.responseText);
            });
        }
    });

    $(document).on("click", ".edit-shared-issue", function (e) {
        $("#shared-issue-dialog").load("/plugins/servlet/pm-dashboard-servlet", { 'shared-issues-id': $(this).data('id') }, function(response, status, xhr) {
            if (status === "error") {
                showMessage("error", response.responseText);
            }
            else {
                $("#shared-issue-dialog").dialog({
                    modal: true,
                    minHeight: 200,
                    title: "Edit shared issue",
                    show: { effect: "fade", duration: 200 },
                    hide: { effect: "fade", duration: 200 },
                    open: function() {
                        $(document).on("click", ".ui-widget-overlay", function () {
                            if ($("#shared-issue-dialog").dialog("isOpen")) {
                                $("#shared-issue-dialog").dialog("close");
                            }
                        });

                        $('#shared-issues-start-date').datepicker({ dateFormat: "yy-mm-dd", firstDay: 1 });
                        $('#shared-issues-end-date').datepicker({ dateFormat: "yy-mm-dd", firstDay: 1 });
                    }
                });
            }
        });
    });

    $(document).on("click", "#show-my-team", function (e) {
        e.preventDefault();
        var dashboard = $("li.aui-nav-selected a");
        $.ajax({
            type: "post",
            url: "/rest/pm-dashboard-rest/1.0/pm-dashboard/showMyTeam"
        }).done(function () {
            $("#pm-dashboard").load(location.href + " #pm-dashboard-inner", function () {
                var contentId = (dashboard.text() === "Assigned issues") ? "assigned-issues-content" : "daily-reports-content";
                navigateTo(dashboard, contentId);
            });
        }).fail(function (error) {
            showMessage("error", error.responseText);
        });
    });

    $(document).on("click", "#assigned-issues-nav-item", function (e) {
        navigateTo(e.target, "assigned-issues-content");
    });

    $(document).on("click", "#daily-reports-nav-item", function (e) {
        navigateTo(e.target, "daily-reports-content");
    });
});

function navigateTo(trigger, contentId) {
    $("#main-nav li").removeClass("aui-nav-selected");
    $(trigger).parent().addClass("aui-nav-selected");
    $(".nav-content").hide();
    $("#" + contentId).show();
}

function showMessage(type, text) {
    if (type === "success") {
        AJS.messages.success("#aui-flag-container", {
            title: text,
            fadeout: true
        });
    }
    else if (type === 'error') {
        AJS.messages.error("#aui-flag-container", {
            title: text,
            fadeout: true
        });
    }
}
