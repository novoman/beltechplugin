AJS.$(function() {
    var today = new Date();
    var year = today.getFullYear();

    var element = document.getElementById('beltech_pm');
    if (element && element.value == "true") {
        AJS.$("#beltech-content").hide();
        AJS.$("#requests-pm-content").show();
        AJS.$("#my-requests-content").hide();
        AJS.$("#aui-uid-devs-years-tabs-" + year).click();
        AJS.$("#aui-uid-pms-years-tabs-" + year).click();
     } else {
        AJS.$("#beltech-content").hide();
        AJS.$("#requests-pm-content").hide();
        AJS.$("#my-requests-content").show();
     }

    AJS.$(document).on("click", "#my-requests-nav-item", function (e) {
        navigateTo(e.target, "my-requests-content");
    });

    AJS.$(document).on("click", "#beltech-nav-item", function (e) {
        navigateTo(e.target, "beltech-content");
    });

    AJS.$(document).on("click", "#requests-pm-nav-item", function (e) {
        navigateTo(e.target, "requests-pm-content");
        AJS.$("#aui-uid-devs-years-tabs-" + year).click();
        AJS.$("#aui-uid-pms-years-tabs-" + year).click();
    });

    function navigateTo(trigger, contentId){
        AJS.$("#main-nav li").removeClass("aui-nav-selected");
        AJS.$(trigger).parent().addClass("aui-nav-selected");
        AJS.$(".nav-content").hide();
        AJS.$("#" + contentId).show();
    }

    AJS.$('#start-date').datepicker({ dateFormat: "yy-mm-dd", firstDay: 1 });
    AJS.$('#end-date').datepicker({ dateFormat: "yy-mm-dd", firstDay: 1 });

    AJS.$(document).on("click", "#add-request", function (e) {
        e.preventDefault();
        AJS.$('#vacations-content').overlayMask();
        AJS.$.ajax({
            type: "post",
            url: "/rest/vacations-rest/1.0/vacations/addUserRequest",
            data: AJS.$("#user-request").serialize()
        }).done(function (data, textStatus) {
            afterAjaxReload("my-requests-nav-item", data);
        }).fail(function (error) {
            showMessage("error", error.responseText);
        });
    });

    AJS.$(document).on("click", ".delete-request", function (e) {
        if (confirm("Are you sure?")) {
            AJS.$('#vacations-content').overlayMask();
            AJS.$.ajax({
                type: "post",
                url: "/rest/vacations-rest/1.0/vacations/deleteUserRequest",
                data: { 'request-id': AJS.$(this).data('id') }
            }).done(function (data, textStatus) {
                afterAjaxReload("my-requests-nav-item", data);
            }).fail(function (error) {
                showMessage("error", error.responseText);
            });
        }
    });

    AJS.$(document).on("click", "#requests-pm-content .approve", function () {
        AJS.$('#requests-pm-content').overlayMask();
        AJS.$.ajax({
            type: "post",
            url: "/rest/vacations-rest/1.0/vacations/approveUserRequest",
            data: { 'request-id': AJS.$(this).data('id'), 'pm-description': AJS.$(this).parents('tr').find('#pm-description').val() }
        }).done(function (data, textStatus) {
            afterAjaxReload("requests-pm-nav-item", data);
        }).fail(function (XMLHttpRequest, textStatus, errorThrown) {
            showMessage("error", "Request error.");
        });

        return false;
    });

    AJS.$(document).on("click", "#requests-pm-content .decline", function () {
        AJS.$('#requests-pm-content').overlayMask();
        AJS.$.ajax({
            type: "post",
            url: "/rest/vacations-rest/1.0/vacations/declineUserRequest",
            data: { 'request-id': AJS.$(this).data('id'), 'pm-description': AJS.$(this).parents('tr').find('#pm-description').val() }
        }).done(function (data, textStatus) {
            afterAjaxReload("requests-pm-nav-item", data);
        }).fail(function (XMLHttpRequest, textStatus, errorThrown) {
            showMessage("error", "Request error.");
        });

        return false;
    });

    AJS.$(document).on("change", "select[name='programmers']", function () {
        var calendar = AJS.$(this).parents(".fieldset").children(".vacations-calendar");
        calendar.load("/plugins/servlet/vacations-calendar-servlet", { userId: AJS.$(this).val() }, function(response, status, xhr) {
            if (status === "error") {
                showMessage("error", response.responseText);
            }
        });
    });

    function afterAjaxReload(tabId, message) {
        AJS.$("#vacations-content").load("/plugins/servlet/vacations-servlet #vacations-content-section", function(response, status, xhr) {
            if (status === "error") {
                showMessage("error", response.responseText);
            }
            else {
                AJS.tabs.setup();
                AJS.$('#start-date').datepicker({ dateFormat: "yy-mm-dd", firstDay: 1 });
                AJS.$('#end-date').datepicker({ dateFormat: "yy-mm-dd", firstDay: 1 });
                AJS.$("#" + tabId).click();
                AJS.$("#" + tabId).addClass("aui-nav-selected");
                showMessage("success", message);
            }
        });
    }

    function showMessage(type, text) {
        if (type === "success") {
            AJS.messages.success("#aui-flag-container", {
                title: text,
                fadeout: true
            });
        }
        else if (type === 'error') {
            AJS.messages.error("#aui-flag-container", {
                title: text,
                fadeout: true
            });
            setTimeout(function() { AJS.$('#vacations-content').overlayMask('hide') }, 500);
        }
    }

    AJS.$.fn.overlayMask = function (action) {
        var mask = this.find('.overlay-mask');
        // Create the required mask
        if (!mask.length) {
            mask = AJS.$('<div class="overlay-mask"><div class="loading"></div></div>');
            mask.appendTo(this);
        }
        // Act based on params
        if (!action || action === 'show') {
            mask.show();
        } else if (action === 'hide') {
            mask.hide();
        }

        return this;
    };
});

function toggleRowGroup(el) {
    var tr = AJS.$(el);
    var n = tr.next();
    tr.toggleClass('open');
    while (n.length && !n.hasClass('group')) {
        n.toggle();
        n = n.next('tr');
    }
}
