function moveOptions(theSelFrom, theSelTo) {
    $(theSelFrom).find('option:selected').detach().prop("selected", false).appendTo($(theSelTo));
}

function make_selects_send_to_server() {
    $("select[name^='manager_'] option").prop('selected', true);
    return false;
}
