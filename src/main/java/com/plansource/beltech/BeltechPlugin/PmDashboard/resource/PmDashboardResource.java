package com.plansource.beltech.BeltechPlugin.PmDashboard.resource;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.issue.Issue;
import com.plansource.beltech.BeltechPlugin.PmDashboard.ao.SharedIssues;
import com.plansource.beltech.BeltechPlugin.WorklogGenerator.servlet.jqlQueryHelper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.Map;

@Path("/pm-dashboard")
public class PmDashboardResource
{
	private ActiveObjects activeObjects;

	public PmDashboardResource(ActiveObjects activeObjects)
	{
		this.activeObjects = activeObjects;
	}

	@POST
	@Consumes("application/x-www-form-urlencoded")
	@Path("/addSharedIssue")
	public Response addSharedIssue(@Context HttpServletRequest request)
	{
		Map<String,String[]> paramMap = request.getParameterMap();
		SharedIssues sharedIssue = null;

		if (request.getParameter("shared-issues-id") != null && !"".equals(request.getParameter("shared-issues-id"))) {
			int requestId = Integer.parseInt(request.getParameter("shared-issues-id"));
			sharedIssue = this.activeObjects.get(SharedIssues.class, requestId);
		}

		String[] userIds = paramMap.get("shared-issues-user-id");
		for (String userId : userIds) {
			Long developerId = Long.parseLong(userId);
			if (developerId > 0) {
				if (sharedIssue == null) {
					sharedIssue = this.activeObjects.create(SharedIssues.class);
				}
				try {
					String issueKey = request.getParameter("shared-issues-issue-key");
					jqlQueryHelper jqlHelper = new jqlQueryHelper();
					Issue issue = jqlHelper.getIssueByKey(issueKey);

					String status = request.getParameter("shared-issues-status");
					String description = request.getParameter("shared-issues-description");

					LocalDate localDate = LocalDate.now();
					Date date = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());

					String startDateParam = request.getParameter("shared-issues-start-date");
					String endDateParam = request.getParameter("shared-issues-end-date");

					if (startDateParam == null || "".equals(startDateParam)) {
						startDateParam = new SimpleDateFormat("yyyy-MM-dd").format(date);
					}

					if (endDateParam == null || "".equals(endDateParam)) {
						endDateParam = "2100-01-01";
					}

					Date startDate = new SimpleDateFormat("yyyy-MM-dd").parse(startDateParam);
					Date endDate = new SimpleDateFormat("yyyy-MM-dd").parse(endDateParam);

					sharedIssue.setUserId(developerId);
					if (issue != null) {
						sharedIssue.setIssueId(issue.getId());
					}
					else {
						return Response.serverError().entity("Can't find issue with key: " + issueKey).build();
					}

					sharedIssue.setStatus(status);
					sharedIssue.setDescription(description);

					sharedIssue.setStartDate(startDate);
					sharedIssue.setEndDate(endDate);
					sharedIssue.setCreatedAt(date);
					sharedIssue.setUpdatedAt(date);

					sharedIssue.save();

					sharedIssue = null;
				}
				catch (ParseException e) {
					return Response.serverError().entity("Can't parse dates.").build();
				}
			}
			else {
				return Response.serverError().entity("Please select a user!").build();
			}
		}

		return Response.ok().entity("Shared issue was added successfully.").build();
	}

	@POST
	@Consumes("application/x-www-form-urlencoded")
	@Path("/deleteSharedIssue")
	public Response deleteSharedIssue(@Context HttpServletRequest request)
	{
		int requestId = Integer.parseInt(request.getParameter("shared-issues-id"));
		SharedIssues sharedIssue = this.activeObjects.get(SharedIssues.class, requestId);

		if (sharedIssue != null) {
			this.activeObjects.delete(sharedIssue);
		}

		return Response.ok().entity("Shared issue was deleted successfully.").build();
	}

	@POST
	@Consumes("application/x-www-form-urlencoded")
	@Path("/showMyTeam")
	public Response showMyTeam(@Context HttpServletRequest request)
	{
		HttpSession session = request.getSession();
		boolean showMyTeam;
		if (session.getAttribute("showMyTeam") == null) {
			showMyTeam = false;
		}
		else {
			showMyTeam = (boolean)session.getAttribute("showMyTeam");
		}
		session.setAttribute("showMyTeam", !showMyTeam);

		return Response.ok().build();
	}
}
