package com.plansource.beltech.BeltechPlugin.PmDashboard.servlet;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.velocity.VelocityManager;
import com.google.common.collect.Maps;
import com.plansource.beltech.BeltechPlugin.DailyReport.DailyReportHelper;
import com.plansource.beltech.BeltechPlugin.GroupsManagement.GroupsManagementHelper;
import com.plansource.beltech.BeltechPlugin.PmDashboard.ao.SharedIssues;
import com.plansource.beltech.BeltechPlugin.Vacations.VacationsHelper;
import com.plansource.beltech.BeltechPlugin.WorklogGenerator.servlet.SortUsers;
import com.plansource.beltech.BeltechPlugin.WorklogGenerator.servlet.jqlQueryHelper;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.util.*;

@Scanned
public class PmDashboardServlet extends HttpServlet
{
    private VelocityManager velocityManager;
    private ActiveObjects activeObjects;
    private WebResourceManager webResourceManager;
    private UserManager userManager;

    public PmDashboardServlet(
        @ComponentImport VelocityManager velocityManager,
        @ComponentImport ActiveObjects activeObjects,
        @ComponentImport WebResourceManager webResourceManager,
        @ComponentImport UserManager userManager
    )
    {
        this.velocityManager = velocityManager;
        this.activeObjects = activeObjects;
        this.webResourceManager = webResourceManager;
        this.userManager = userManager;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException
    {
        if (!"PM".equals(VacationsHelper.getCurrentUserRole())) {
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
        }

        ApplicationUser currentUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
        Map<String, Object> context = Maps.newHashMap();

        if (currentUser != null) {
            jqlQueryHelper jqlHelper = new jqlQueryHelper();
            GroupsManagementHelper groupsManagementHelper = new GroupsManagementHelper(activeObjects, userManager);
            List<ApplicationUser> developers = groupsManagementHelper.getUsersByRole("devs");
            DailyReportHelper dailyReportHelper = new DailyReportHelper(activeObjects, userManager);

            HttpSession session = request.getSession();
            boolean showMyTeam;
            if (session.getAttribute("showMyTeam") == null) {
                showMyTeam = false;
            }
            else {
                showMyTeam = (boolean)session.getAttribute("showMyTeam");
            }

            List<ApplicationUser> myTeam = groupsManagementHelper.getDevelopersByPm(currentUser);
            List<ApplicationUser> unavailableToday = dailyReportHelper.findUnavailableTodayDevelopers(developers);
            List<ApplicationUser> unavailableTomorrow = dailyReportHelper.findUnavailableTomorrowDevelopers(developers);

            List<ApplicationUser> availableTodayDevelopers = new ArrayList<>();
            List<ApplicationUser> unavailableTodayDevelopers = new ArrayList<>();

            List<ApplicationUser> availableTomorrowDevelopers = new ArrayList<>();
            List<ApplicationUser> unavailableTomorrowDevelopers = new ArrayList<>();

            for (ApplicationUser developer : developers) {
                if (unavailableTomorrow.contains(developer)) {
                    if (!showMyTeam || (myTeam.contains(developer) || dailyReportHelper.hasPmAssignedIssue(developer, currentUser, false))) {
                        unavailableTomorrowDevelopers.add(developer);
                    }
                }
                else if (!showMyTeam || (myTeam.contains(developer) || dailyReportHelper.hasPmAssignedIssue(developer, currentUser, false))) {
                    availableTomorrowDevelopers.add(developer);
                }

                if (unavailableToday.contains(developer)) {
                    if (!showMyTeam || (myTeam.contains(developer) || dailyReportHelper.hasPmAssignedIssue(developer, currentUser, true))) {
                        unavailableTodayDevelopers.add(developer);
                    }
                }
                else if (!showMyTeam || (myTeam.contains(developer) || dailyReportHelper.hasPmAssignedIssue(developer, currentUser, true))) {
                    availableTodayDevelopers.add(developer);
                }
            }

            context.put("jqlHelper", jqlHelper);
            context.put("dailyReportHelper", dailyReportHelper);
            context.put("groupsManagementHelper", groupsManagementHelper);
            context.put("showMyTeam", showMyTeam);
            context.put("developers", SortUsers.sortByDisplayName(developers));

            context.put("availableTodayDevelopers", SortUsers.sortByDisplayName(availableTodayDevelopers));
            context.put("availableTomorrowDevelopers", SortUsers.sortByDisplayName(availableTomorrowDevelopers));

            context.put("unavailableTodayDevelopers", SortUsers.sortByDisplayName(unavailableTodayDevelopers));
            context.put("unavailableTomorrowDevelopers", SortUsers.sortByDisplayName(unavailableTomorrowDevelopers));
        }
        AvatarService avatarService = ComponentAccessor.getAvatarService();
        context.put("avatarService", avatarService);
        context.put("currentUser", currentUser);
        context.put("avatarSize", Avatar.Size.defaultSize());

        this.webResourceManager.requireResource("com.plansource.beltech.BeltechPlugin:jquery-ui-resources");
        this.webResourceManager.requireResource("com.plansource.beltech.BeltechPlugin:pm-dashboard-resources");
        String content = this.velocityManager.getEncodedBody("/templates/", "pm-dashboard.vm", "UTF-8", context);
        response.setContentType("text/html;charset=utf-8");
        response.getWriter().write(content);
        response.getWriter().close();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        if (!"PM".equals(VacationsHelper.getCurrentUserRole())) {
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
        }

        if (request.getParameter("shared-issues-id") == null || "".equals(request.getParameter("shared-issues-id"))) {
            return;
        }

		Map<String, Object> context = Maps.newHashMap();

		int requestId = Integer.parseInt(request.getParameter("shared-issues-id"));
		SharedIssues sharedIssue = this.activeObjects.get(SharedIssues.class, requestId);
        GroupsManagementHelper groupsManagementHelper = new GroupsManagementHelper(this.activeObjects, this.userManager);
        List<ApplicationUser> developers = groupsManagementHelper.getUsersByRole("devs");
        jqlQueryHelper jqlHelper = new jqlQueryHelper();

		if (sharedIssue != null) {
			context.put("sharedIssue", sharedIssue);
		}
        context.put("developers", SortUsers.sortByDisplayName(developers));
        context.put("jqlHelper", jqlHelper);

		this.webResourceManager.requireResource("com.plansource.beltech.BeltechPlugin:jquery-ui-resources");
		this.webResourceManager.requireResource("com.plansource.beltech.BeltechPlugin:pm-dashboard-resources");
		String content = this.velocityManager.getEncodedBody("/templates/pm-dashboard/", "shared-issue-modal.vm", "UTF-8", context);
		response.setContentType("text/html;charset=utf-8");
		response.getWriter().write(content);
		response.getWriter().close();
    }
}
