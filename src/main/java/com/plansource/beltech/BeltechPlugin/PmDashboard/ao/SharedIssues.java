package com.plansource.beltech.BeltechPlugin.PmDashboard.ao;

import net.java.ao.Entity;
import net.java.ao.schema.StringLength;
import java.util.Date;

public interface SharedIssues extends Entity
{
	Long getUserId();
	void setUserId(Long userId);

	Long getIssueId();
	void setIssueId(Long issueId);

	String getStatus();
	void setStatus(String status);

	@StringLength(StringLength.UNLIMITED)
	String getDescription();
	void setDescription(String description);

	Date getStartDate();
	void setStartDate(Date date);

	Date getEndDate();
	void setEndDate(Date date);

	Date getCreatedAt();
	void setCreatedAt(Date date);

	Date getUpdatedAt();
	void setUpdatedAt(Date date);
}
