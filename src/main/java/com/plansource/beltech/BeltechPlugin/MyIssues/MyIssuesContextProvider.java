package com.plansource.beltech.BeltechPlugin.MyIssues;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.web.ContextProvider;
import com.google.common.collect.Maps;

import java.util.Map;

@Scanned
public class MyIssuesContextProvider implements ContextProvider {
    private final PluginAccessor pluginAccessor;

    public MyIssuesContextProvider(@ComponentImport PluginAccessor pluginAccessor)
    {
        this.pluginAccessor = pluginAccessor;
    }

    @Override
    public void init(final Map<String, String> params) throws PluginParseException {
    }

    @Override
    public Map<String, Object> getContextMap(final Map<String, Object> context) {
        final Map<String, Object> newContext = Maps.newHashMap(context);
        Plugin plugin = pluginAccessor.getEnabledPlugin("com.plansource.beltech.BeltechPlugin");
        newContext.put("pluginName", plugin.getName());

        return newContext;
    }
}
