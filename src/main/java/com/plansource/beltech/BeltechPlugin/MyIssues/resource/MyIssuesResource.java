package com.plansource.beltech.BeltechPlugin.MyIssues.resource;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.json.JSONArray;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.plansource.beltech.BeltechPlugin.DailyReport.DailyReportHelper;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.*;

@Path("/my-issues")
public class MyIssuesResource
{
	private ActiveObjects activeObjects;
	private UserManager userManager;

	public MyIssuesResource(ActiveObjects activeObjects, UserManager userManager)
	{
		this.activeObjects = activeObjects;
		this.userManager = userManager;
	}

	@GET
	@Path("/get")
	@Produces({MediaType.APPLICATION_JSON})
	public Response generate(@Context HttpServletRequest request) throws JSONException {
		ApplicationUser currentUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
		DailyReportHelper dailyReportHelper = new DailyReportHelper(activeObjects, userManager);
		List<Issue> issues = dailyReportHelper.getAllActiveIssuesForUser(currentUser);

		return Response.ok(makeJSONFromIssuesList(issues).toString()).build();
	}

	private JSONObject makeJSONFromIssuesList(List<Issue> issues) throws JSONException {
		ApplicationUser currentUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
		JSONObject responseJson = new JSONObject();
		JSONArray jsonArray = new JSONArray();

		for (Issue issue : issues) {
			JSONObject formDetailsJson = new JSONObject();
			formDetailsJson.put("id", issue.getId());

			JSONObject fields = new JSONObject();
			JSONObject issuetype = new JSONObject();

			String issueTypeName = "";
			if (issue.getIssueType() != null) {
				issueTypeName = issue.getIssueType().getName();
			}
			issuetype.put("name", issueTypeName);

			String issueTypeIconUrl = "";
			if (issue.getIssueType() != null) {
				issueTypeIconUrl = issue.getIssueType().getIconUrl();
			}
			issuetype.put("iconUrl", issueTypeIconUrl);

			JSONObject priority = new JSONObject();

			String priorityName = "";
			if (issue.getPriority() != null) {
				priorityName = issue.getPriority().getName();
			}
			priority.put("name", priorityName);

			String priorityIconUrl = "";
			if (issue.getPriority() != null) {
				priorityIconUrl = issue.getPriority().getIconUrl();
			}
			priority.put("iconUrl", priorityIconUrl);

			fields.put("issuetype", issuetype);
			fields.put("summary", issue.getSummary());
			fields.put("priority", priority);

			boolean isShared = false;
			if (!currentUser.equals(issue.getAssignee())) {
				isShared = true;
			}

			formDetailsJson.put("key", issue.getKey());
			formDetailsJson.put("fields", fields);
			formDetailsJson.put("isShared", isShared);

			jsonArray.put(formDetailsJson);
		}

		return responseJson.put("issues", jsonArray);
	}
}
