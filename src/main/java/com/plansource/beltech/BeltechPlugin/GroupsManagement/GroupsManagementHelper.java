package com.plansource.beltech.BeltechPlugin.GroupsManagement;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.plansource.beltech.BeltechPlugin.GroupsManagement.ao.GroupsManagement;
import com.plansource.beltech.BeltechPlugin.Vacations.VacationsHelper;

import java.util.*;
import java.util.stream.Stream;

public class GroupsManagementHelper {

    private ActiveObjects activeObjects;
    private UserManager userManager;
    public List<ApplicationUser> unselectedDevelopers;
    private GroupsManagement[] allGroupsManagement;

    public GroupsManagementHelper(ActiveObjects activeObjects, UserManager userManager)
    {
        this.activeObjects = activeObjects;
        this.userManager = userManager;
        this.allGroupsManagement = getAllGroupsManagement();
        this.unselectedDevelopers = getUnselectedDevelopers();
    }

    public List<ApplicationUser> getUsersByRole(String userRole) {
        if (userRole == null) {
            return null;
        }

        GroupManager groupManager = ComponentAccessor.getGroupManager();
        List<ApplicationUser> applicationUsers = new ArrayList<>();

        if (userRole.equals("pms")) {
            applicationUsers.addAll(groupManager.getUsersInGroup(VacationsHelper.beltechUserRoles.get("pm")));
        }
        else if (userRole.equals("devs")) {
            applicationUsers.addAll(groupManager.getUsersInGroup(VacationsHelper.beltechUserRoles.get("dev")));
        }
        applicationUsers.removeIf(user -> (!user.isActive() || user.getDisplayName().equals("Igor Motro")));

        return applicationUsers;
    }

    public List<ApplicationUser> getDevelopersByPm(ApplicationUser pm)
    {
        if (pm == null) {
            return null;
        }

        List<ApplicationUser> developers = new ArrayList<>();
        GroupsManagement[] groupsManagements = Stream.of(this.allGroupsManagement).filter((gm) -> pm.getId().equals(gm.getPmId())).toArray(GroupsManagement[]::new);

        for (GroupsManagement gr : groupsManagements) {
            if (this.userManager.getUserById(gr.getDeveloperId()).isPresent()) {
                ApplicationUser developer = this.userManager.getUserById(gr.getDeveloperId()).get();
                if (developer.isActive()) {
                    developers.add(developer);
                }
            }
        }

        return developers;
    }

    private List<ApplicationUser> getUnselectedDevelopers()
    {
        List<ApplicationUser> pms =  getUsersByRole("pms");
        List<ApplicationUser> developers = getUsersByRole("devs");

        for (ApplicationUser pm : pms) {
            developers.removeAll(getDevelopersByPm(pm));
        }

        return developers;
    }

    private GroupsManagement[] getAllGroupsManagement()
    {
        return this.activeObjects.find(GroupsManagement.class);
    }
}
