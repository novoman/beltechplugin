package com.plansource.beltech.BeltechPlugin.GroupsManagement.servlet;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.webresource.WebResourceManager;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.plansource.beltech.BeltechPlugin.GroupsManagement.GroupsManagementHelper;
import com.atlassian.velocity.VelocityManager;
import com.google.common.collect.Maps;
import com.plansource.beltech.BeltechPlugin.GroupsManagement.ao.GroupsManagement;
import com.plansource.beltech.BeltechPlugin.Vacations.VacationsHelper;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.Set;

@Scanned
public class GroupsManagementServlet extends HttpServlet
{
    private VelocityManager velocityManager;
    private ActiveObjects activeObjects;
    private WebResourceManager webResourceManager;
    private UserManager userManager;

    public GroupsManagementServlet(
        @ComponentImport VelocityManager velocityManager,
        @ComponentImport ActiveObjects activeObjects,
        @ComponentImport WebResourceManager webResourceManager,
        @ComponentImport UserManager userManager
    )
    {
        this.velocityManager = velocityManager;
        this.activeObjects = activeObjects;
        this.webResourceManager = webResourceManager;
        this.userManager = userManager;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException
    {
        if (!"PM".equals(VacationsHelper.getCurrentUserRole())) {
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
        }

        ApplicationUser currentUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
        Map<String, Object> context = Maps.newHashMap();

        if (currentUser != null) {
            GroupsManagementHelper groupsManagementHelper = new GroupsManagementHelper(this.activeObjects, this.userManager);

            context.put("groupsManagementHelper", groupsManagementHelper);
            context.put("unselectedDevelopers", groupsManagementHelper.unselectedDevelopers);
        }

        this.webResourceManager.requireResource("com.plansource.beltech.BeltechPlugin:groups-management-resources");
        String content = this.velocityManager.getEncodedBody("/templates/", "groups.vm", "UTF-8", context);
        response.setContentType("text/html;charset=utf-8");
        response.getWriter().write(content);
        response.getWriter().close();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException
    {
        Map<String,String[]> paramMap = request.getParameterMap();
        Set<String> params =  paramMap.keySet();
        GroupsManagement[] entities = this.activeObjects.find(GroupsManagement.class);

        if (entities != null) {
            this.activeObjects.delete(entities);
        }

        for (String param : params) {
            if (param != null && param.contains("manager_")) {
                Long managerId = Long.parseLong(param.substring(8));
                if (managerId > 0) {
                    String[] paramValues = paramMap.get(param);
                    for (String paramValue : paramValues) {
                        if (paramValue == null || paramValue.isEmpty()) {
                            continue;
                        }
                        Long developerId = Long.parseLong(paramValue);
                        if (developerId > 0) {
                            Date date = new Date();

                            GroupsManagement groupsManagement = this.activeObjects.create(GroupsManagement.class);
                            groupsManagement.setPmId(managerId);
                            groupsManagement.setDeveloperId(developerId);
                            groupsManagement.setCreatedAt(date);
                            groupsManagement.setUpdatedAt(date);
                            groupsManagement.save();
                        }
                    }
                }
            }
        }

        doGet(request, response);
    }
}
