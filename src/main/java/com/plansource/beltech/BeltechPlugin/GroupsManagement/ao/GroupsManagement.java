package com.plansource.beltech.BeltechPlugin.GroupsManagement.ao;

import net.java.ao.Entity;
import java.util.Date;

public interface GroupsManagement extends Entity
{
	Long getPmId();
	void setPmId(Long userId);

	Long getDeveloperId();
	void setDeveloperId(Long userId);

	Date getCreatedAt();
	void setCreatedAt(Date date);

	Date getUpdatedAt();
	void setUpdatedAt(Date date);
}
