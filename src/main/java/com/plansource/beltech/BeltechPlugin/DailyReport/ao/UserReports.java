package com.plansource.beltech.BeltechPlugin.DailyReport.ao;

import net.java.ao.Entity;
import net.java.ao.schema.StringLength;
import java.util.Date;

public interface UserReports extends Entity
{
	Long getUserId();
	void setUserId(Long userId);

	@StringLength(StringLength.UNLIMITED)
	String getContents();
	void setContents(String contents);

	Date getCreatedAt();
	void setCreatedAt(Date date);

	Date getUpdatedAt();
	void setUpdatedAt(Date date);
}
