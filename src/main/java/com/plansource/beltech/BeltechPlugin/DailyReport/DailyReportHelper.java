package com.plansource.beltech.BeltechPlugin.DailyReport;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.plansource.beltech.BeltechPlugin.DailyReport.ao.UserReports;
import com.plansource.beltech.BeltechPlugin.PmDashboard.ao.SharedIssues;
import com.plansource.beltech.BeltechPlugin.Vacations.ao.UserRequests;
import com.plansource.beltech.BeltechPlugin.WorklogGenerator.servlet.jqlQueryHelper;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.ArrayUtils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DailyReportHelper {

    final double WORKDAY_LENGTH = 8.0;

    private ActiveObjects activeObjects;
    private UserManager userManager;
    private UserReports[] allUserReports;
    private SharedIssues[] allSharedIssues;
    private UserRequests[] allUserRequests;

    public DailyReportHelper(ActiveObjects activeObjects, UserManager userManager)
    {
        this.activeObjects = activeObjects;
        this.userManager = userManager;
        this.allUserReports = getAllUserReports();
        this.allSharedIssues = getAllSharedIssues();
        this.allUserRequests = getAllUserRequests();
    }

    private SharedIssues[] getAllSharedIssues()
    {
        LocalDate localDate = LocalDate.now();
        Date date = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());

        return Stream.of(this.activeObjects.find(SharedIssues.class)).filter((si) -> (si.getStartDate() != null && si.getStartDate().compareTo(date) <= 0) && (si.getEndDate() != null && si.getEndDate().compareTo(date) >= 0)).toArray(SharedIssues[]::new);
    }

    private UserReports[] getAllUserReports()
    {
        LocalDate localDate = LocalDate.now();
        Date date = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());

        return Stream.of(this.activeObjects.find(UserReports.class)).filter((ur) -> (ur.getCreatedAt() != null && ur.getCreatedAt().compareTo(atStartOfDay(date)) >= 0) && (ur.getCreatedAt() != null && ur.getCreatedAt().compareTo(atEndOfDay(date)) <= 0)).toArray(UserReports[]::new);
    }

    public UserReports getReportForUser(ApplicationUser user) {
        UserReports userReport = null;

        if (this.allUserReports == null || user == null) {
            return null;
        }

        UserReports[] userReports = Stream.of(this.allUserReports).filter((ur) -> (user.getId().equals(ur.getUserId()))).toArray(UserReports[]::new);

        if (userReports.length > 0) {
            userReport = userReports[0];
        }

        return userReport;
    }

    public String getSharedIssuesIdsStringForUser(ApplicationUser user)
    {
        String result = "";

        if (this.allSharedIssues == null || user == null) {
            return "";
        }

        SharedIssues[] sharedIssues = Stream.of(this.allSharedIssues).filter((si) -> (user.getId().equals(si.getUserId()))).toArray(SharedIssues[]::new);

        for (SharedIssues sharedIssue : sharedIssues) {
            result += sharedIssue.getIssueId() + ", ";
        }

        return result.equals("") ? result : result.substring(0, result.length() - 2);
    }

    public SharedIssues getSharedIssueRecordByUserAndIssue(ApplicationUser user, Issue issue)
    {
        if (this.allSharedIssues == null || user == null || issue == null) {
            return null;
        }

        SharedIssues[] sharedIssues = Stream.of(this.allSharedIssues).filter((si) -> (user.getId().equals(si.getUserId()) && issue.getId().equals(si.getIssueId()))).toArray(SharedIssues[]::new);

        if (sharedIssues.length > 0) {
            return sharedIssues[0];
        }

        return null;
    }

    public double getUnoccupiedHoursForUser(ApplicationUser user)
    {
        if (user == null) {
            return 0.0;
        }

        double totalHours = 0.0;
        List<Issue> issues = getAllActiveIssuesForUser(user);

        for (Issue issue : issues) {
            totalHours += getETAForIssue(issue);
        }

        return totalHours >= WORKDAY_LENGTH ? 0.0 : WORKDAY_LENGTH - totalHours;
    }

    public double getETAForIssue(Issue issue)
    {
        return issue != null && issue.getEstimate() != null ? issue.getEstimate() / 3600.0 : 0.0;
    }

    public String[][] getContentsValuesFromReport(UserReports userReport)
    {
        String[][] contentsValues = null;

        if (userReport != null) {
            ByteArrayInputStream in;
            try {
                in = new ByteArrayInputStream(Hex.decodeHex(userReport.getContents().toCharArray()));
                contentsValues = (String[][]) new ObjectInputStream(in).readObject();
            } catch (DecoderException | ClassNotFoundException | IOException e) {
                e.printStackTrace();
            }
        }

        return contentsValues;
    }


    public String getNotesFromReport(UserReports userReport)
    {
        String notes = "";
        if (userReport != null) {
            String[][] contentsValues = getContentsValuesFromReport(userReport);
            if (contentsValues != null) {
                notes = contentsValues[0][0];
            }
        }

        return notes;
    }

    public boolean checkForCheckboxes(UserReports userReport)
    {
        if (userReport != null) {
            String[][] contentsValues = getContentsValuesFromReport(userReport);
            if (contentsValues != null) {
                List<String> needToTalkIssues = Arrays.asList(contentsValues[1]);

                if (needToTalkIssues.size() > 0) {
                    return true;
                }

                List<String> goingToFinishIssues = Arrays.asList(contentsValues[2]);
                if (this.userManager.getUserById(userReport.getUserId()).isPresent()) {
                    ApplicationUser user = this.userManager.getUserById(userReport.getUserId()).get();
                    List<Issue> issues = getAllIssuesWithActivityForUser(user);

                    for (Issue issue : issues) {
                        if (!goingToFinishIssues.contains(issue.getId().toString())) {
                            return false;
                        }
                    }
                }
            }
        }

        return true;
    }

    public List<Issue> getAllActiveIssuesForUser(ApplicationUser user)
    {
        jqlQueryHelper jql = new jqlQueryHelper();

        List<Issue> issues = new ArrayList<>();
        List<Issue> userIssues = jql.getActiveIssuesForUser(user);
        List<Issue> sharedIssues = jql.getActiveSharedIssuesByIds(getSharedIssuesIdsStringForUser(user));
        issues.addAll(userIssues);
        issues.addAll(sharedIssues);

        return issues.stream().distinct().collect(Collectors.toList());
    }

    public List<Issue> getAllIssuesWithActivityForUser(ApplicationUser user)
    {
        jqlQueryHelper jql = new jqlQueryHelper();

        List<Issue> issues = new ArrayList<>();
        List<Issue> userIssues = jql.getActiveIssuesForUser(user);
        List<Issue> sharedIssues = jql.getActiveSharedIssuesByIds(getSharedIssuesIdsStringForUser(user));
        List<Issue> activityIssues = jql.getActivityIssuesForTodayForUser(user);
        issues.addAll(userIssues);
        issues.addAll(sharedIssues);
        issues.addAll(activityIssues);

        return issues.stream().distinct().collect(Collectors.toList());
    }

    public List<Issue> getOnlyIssuesWithActivityForUser(ApplicationUser user)
    {
        jqlQueryHelper jql = new jqlQueryHelper();

        List<Issue> userIssues = jql.getActiveIssuesForUser(user);
        List<Issue> sharedIssues = jql.getActiveSharedIssuesByIds(getSharedIssuesIdsStringForUser(user));
        List<Issue> activityIssues = jql.getActivityIssuesForTodayForUser(user);
        List<Issue> issues = new ArrayList<>(activityIssues);
        issues.removeAll(userIssues);
        issues.removeAll(sharedIssues);

        return issues.stream().distinct().collect(Collectors.toList());
    }

    public boolean hasPmAssignedIssue(ApplicationUser developer, ApplicationUser pm, boolean forDailyReports)
    {
        if (developer == null || pm == null) {
            return false;
        }

        List<Issue> issues = forDailyReports ? getAllIssuesWithActivityForUser(developer) : getAllActiveIssuesForUser(developer);
        for (Issue issue : issues) {
            if (pm.equals(getPmForIssue(issue))) {
                return true;
            }
        }

        return false;
    }

    public boolean canHavePmDailyReportIssue(ApplicationUser developer)
    {
        if (developer == null) {
            return false;
        }

        List<Issue> issues = getAllIssuesWithActivityForUser(developer);
        for (Issue issue : issues) {
            if (getPmForIssue(issue) == null) {
                return true;
            }
        }

        return false;
    }

    private ApplicationUser getPmForIssue(Issue issue)
    {
        if (issue == null) {
            return null;
        }

        CustomField pmField = getCustomFieldByName(issue, "BelTech PM");

        if (pmField != null) {
            return (ApplicationUser) issue.getCustomFieldValue(pmField);
        }

        return null;
    }

    public boolean wantsToTalk(UserReports userReport)
    {
        if (userReport != null) {
            String[][] contentsValues = getContentsValuesFromReport(userReport);
            if (contentsValues != null) {
                String[] needToTalkIssues = contentsValues[1];

                return needToTalkIssues.length > 0;
            }
        }

        return false;
    }

    public boolean isIssueGoingToFinish(UserReports userReport, Issue issue)
    {
        if (userReport != null && issue != null) {
            String[][] contentsValues = getContentsValuesFromReport(userReport);
            if (contentsValues != null) {
                String[] goingToFinishIssues = contentsValues[2];

                return Arrays.asList(goingToFinishIssues).contains(issue.getId().toString());
            }
        }

        return false;
    }

    public String getBeltechPmName(Issue issue, boolean fullName)
    {
        String result = "";

        if (issue == null) {
            return "--";
        }

        ApplicationUser pm = getPmForIssue(issue);

        if (pm != null) {
            String pmDisplayName = pm.getDisplayName();
            if (pmDisplayName.length() > 0) {
                if (fullName) {
                    return pmDisplayName;
                }

                String[] words = pmDisplayName.split(" ");
                for (String word : words) {
                    result += Character.toUpperCase(word.charAt(0));
                }
            }
            else {
                return "--";
            }
        }
        else {
            return "--";
        }

        return result;
    }

    public CustomField getCustomFieldByName(final Issue issue, final String name)
    {
        if (name == null || issue == null) {
            return null;
        }

        Collection<CustomField>  fields =  ComponentAccessor.getCustomFieldManager().getCustomFieldObjectsByName(name);

        if (fields == null) {
            return null;
        }

        for (CustomField field:fields) {
            if (field.getRelevantConfig(issue) != null)
                return field;

            FieldConfig config = field.getRelevantConfig(issue);

            if (config != null)
                return field;
        }

        return null;
    }

    public static Date atStartOfDay(Date date)
    {
        if (date == null) {
            date = new Date();
        }

        LocalDateTime localDateTime = dateToLocalDateTime(date);
        LocalDateTime startOfDay = localDateTime.with(LocalTime.MIN);

        return localDateTimeToDate(startOfDay);
    }

    public static Date atEndOfDay(Date date)
    {
        if (date == null) {
            date = new Date();
        }

        LocalDateTime localDateTime = dateToLocalDateTime(date);
        LocalDateTime endOfDay = localDateTime.with(LocalTime.MAX);

        return localDateTimeToDate(endOfDay);
    }

    private static LocalDateTime dateToLocalDateTime(Date date)
    {
        if (date == null) {
            date = new Date();
        }

        return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
    }

    private static Date localDateTimeToDate(LocalDateTime localDateTime)
    {
        if (localDateTime == null) {
            localDateTime = LocalDateTime.now();
        }

        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    private LocalDate dateToLocalDate(Date date)
    {
        if (date == null) {
            date = new Date();
        }

        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

    private Date localDateToDate(LocalDate localDate)
    {
        if (localDate == null) {
            localDate = LocalDate.now();
        }

        return Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    private UserRequests[] getAllUserRequests()
    {
        return this.activeObjects.find(UserRequests.class);
    }

    public UserRequests findTodayUserRequestForUser(ApplicationUser user)
    {
        if (user == null) {
            return null;
        }

        LocalDate localDate = LocalDate.now();

        return findUserRequestForUserAndDate(user, localDateToDate(localDate));
    }

    public UserRequests findTomorrowUserRequestForUser(ApplicationUser user)
    {
        if (user == null) {
            return null;
        }

        LocalDate localDate = LocalDate.now();

        return findUserRequestForUserAndDate(user, nextBusinessDay(localDateToDate(localDate)));
    }

    private UserRequests findUserRequestForUserAndDate(ApplicationUser user, Date date)
    {
        if (user == null || date == null) {
            return null;
        }

        UserRequests[] userRequests = new UserRequests[0];

        userRequests = ArrayUtils.addAll(userRequests, Stream.of(this.allUserRequests).filter((ur) -> (ur.getStartDate() != null && ur.getStartDate().compareTo(date) <= 0) && (ur.getEndDate() != null && ur.getEndDate().compareTo(date) >= 0) && UserRequests.RequestResults.approved.equals(ur.getApproveResult()) && user.getId().equals(ur.getRequestById())).toArray(UserRequests[]::new));
        userRequests = ArrayUtils.addAll(userRequests, Stream.of(this.allUserRequests).filter((ur) -> (ur.getStartDate() != null && ur.getStartDate().compareTo(date) == 0) && UserRequests.RequestTypes.working_day_transfer.equals(ur.getRequestType()) && UserRequests.RequestResults.approved.equals(ur.getApproveResult()) && user.getId().equals(ur.getRequestById())).toArray(UserRequests[]::new));

        if (userRequests.length > 0) {
            return userRequests[0];
        }
        else {
            return null;
        }
    }

    public String getUserRequestTitle(UserRequests userRequest)
    {
        String event = "";
        String date;

        if (userRequest == null) {
            return "";
        }

        switch (userRequest.getRequestType()) {
            case working_day_transfer:
            case day_off:
                event += "has Day off";
                break;
            case sick_day:
                event += "has Sick Days";
                break;
            case vacation:
                event += "is on Vacation";
                break;
        }

        Date firstWorkDate = UserRequests.RequestTypes.working_day_transfer.equals(userRequest.getRequestType()) ? userRequest.getStartDate() : userRequest.getEndDate();

        LocalDate today = LocalDate.now();
        firstWorkDate = nextBusinessDay(firstWorkDate);

        if (today.plusDays(1).compareTo(dateToLocalDate(firstWorkDate)) == 0) {
            date = "tomorrow";
        }
        else {
            date = today.plusDays(7).compareTo(dateToLocalDate(firstWorkDate)) > 0 ? new SimpleDateFormat("EEEEE").format(firstWorkDate) : new SimpleDateFormat("MMMMM dd").format(firstWorkDate);
        }

        return event + " till " + date;
    }

    private Date nextBusinessDay(Date d)
    {
        d = addDays(d, 1);

        while (isHoliday(d)) {
            d = addDays(d, 1);
        }

        return d;
    }

    private Date addDays(Date d, int days)
    {
        if (d == null) {
            d = new Date();
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(d);
        cal.add(Calendar.DATE, days);

        return cal.getTime();
    }

    private boolean isHoliday(Date d)
    {
        if (d == null) {
            d = new Date();
        }

        Calendar c = new GregorianCalendar();
        c.setTime(d);

        return (Calendar.SATURDAY == c.get(Calendar.DAY_OF_WEEK)) || (Calendar.SUNDAY == c.get(Calendar.DAY_OF_WEEK));
    }

    public List<ApplicationUser> findUnavailableTodayDevelopers(List<ApplicationUser> developers)
    {
        if (developers == null) {
            return null;
        }

        LocalDate localDate = LocalDate.now();

        return getUnavailableDevelopersForDate(developers, localDateToDate(localDate));
    }

    public List<ApplicationUser> findUnavailableTomorrowDevelopers(List<ApplicationUser> developers)
    {
        if (developers == null) {
            return null;
        }

        LocalDate localDate = LocalDate.now();

        return getUnavailableDevelopersForDate(developers, nextBusinessDay(localDateToDate(localDate)));
    }

    private List<ApplicationUser> getUnavailableDevelopersForDate(List<ApplicationUser> developers, Date date)
    {
        if (developers == null || date == null) {
            return null;
        }

        List<ApplicationUser> unavailableDevelopers = new ArrayList<>();

        for (ApplicationUser developer : developers) {
            UserRequests[] userRequests = new UserRequests[0];

            userRequests = ArrayUtils.addAll(userRequests, Stream.of(this.allUserRequests).filter((ur) -> (ur.getStartDate() != null && ur.getStartDate().compareTo(date) <= 0) && (ur.getEndDate() != null && ur.getEndDate().compareTo(date) >= 0) && UserRequests.RequestResults.approved.equals(ur.getApproveResult()) && developer.getId().equals(ur.getRequestById())).toArray(UserRequests[]::new));
            userRequests = ArrayUtils.addAll(userRequests, Stream.of(this.allUserRequests).filter((ur) -> (ur.getStartDate() != null && ur.getStartDate().compareTo(date) == 0) && UserRequests.RequestTypes.working_day_transfer.equals(ur.getRequestType()) && UserRequests.RequestResults.approved.equals(ur.getApproveResult()) && developer.getId().equals(ur.getRequestById())).toArray(UserRequests[]::new));

            if (userRequests.length > 0) {
                unavailableDevelopers.add(developer);
            }
        }

        return unavailableDevelopers;
    }

    public String isDisabledFont(boolean showMyTeam, Issue issue)
    {
        if (issue == null) {
            return "";
        }

        String result = "";
        ApplicationUser currentUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
        ApplicationUser pm = getPmForIssue(issue);

        if (pm != null) {
            if (!currentUser.equals(pm) && showMyTeam) {
                result = "disabled-font";
            }
        }

        return result;
    }
}
