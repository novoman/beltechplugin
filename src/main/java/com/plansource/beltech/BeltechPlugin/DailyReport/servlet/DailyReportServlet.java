package com.plansource.beltech.BeltechPlugin.DailyReport.servlet;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.velocity.VelocityManager;
import com.google.common.collect.Maps;
import com.plansource.beltech.BeltechPlugin.DailyReport.DailyReportHelper;
import com.plansource.beltech.BeltechPlugin.DailyReport.ao.UserReports;
import com.plansource.beltech.BeltechPlugin.Vacations.VacationsHelper;
import org.apache.commons.codec.binary.Hex;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;

@Scanned
public class DailyReportServlet extends HttpServlet
{
    private VelocityManager velocityManager;
    private ActiveObjects activeObjects;
    private WebResourceManager webResourceManager;
    private UserManager userManager;

    public DailyReportServlet(
        @ComponentImport VelocityManager velocityManager,
        @ComponentImport ActiveObjects activeObjects,
        @ComponentImport WebResourceManager webResourceManager,
        @ComponentImport UserManager userManager
    )
    {
        this.velocityManager = velocityManager;
        this.activeObjects = activeObjects;
        this.webResourceManager = webResourceManager;
        this.userManager = userManager;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException
    {
        if (!"DEV".equals(VacationsHelper.getCurrentUserRole())) {
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
        }

        ApplicationUser currentUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
        Map<String, Object> context = Maps.newHashMap();

        if (currentUser != null) {
            DailyReportHelper dailyReportHelper = new DailyReportHelper(activeObjects, userManager);
            List<Issue> issues = dailyReportHelper.getAllIssuesWithActivityForUser(currentUser);
            UserReports userReport = dailyReportHelper.getReportForUser(currentUser);

            if (userReport != null) {
                String[][] contentsValues = dailyReportHelper.getContentsValuesFromReport(userReport);
                if (contentsValues != null) {
                    String notes = contentsValues[0][0];
                    String[] needToTalkIssues = contentsValues[1];
                    String[] goingToFinishIssues = contentsValues[2];
                    context.put("notes", notes);
                    context.put("needToTalkIssues", Arrays.asList(needToTalkIssues));
                    context.put("goingToFinishIssues", Arrays.asList(goingToFinishIssues));
                }
            }

            context.put("issues", issues);
            context.put("dailyReportHelper", dailyReportHelper);
            context.put("userReport", userReport);
        }

        this.webResourceManager.requireResource("com.plansource.beltech.BeltechPlugin:daily-report-resources");
        String content = this.velocityManager.getEncodedBody("/templates/", "daily-report.vm", "UTF-8", context);
        response.setContentType("text/html;charset=utf-8");
        response.getWriter().write(content);
        response.getWriter().close();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException
    {
        Map<String,String[]> paramMap = request.getParameterMap();

        ApplicationUser currentUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
        DailyReportHelper dailyReportHelper = new DailyReportHelper(activeObjects, userManager);
        UserReports userReport = dailyReportHelper.getReportForUser(currentUser);
        if (userReport == null) {
            userReport = this.activeObjects.create(UserReports.class);
        }

        String[] notes = paramMap.get("notes");
        if (notes == null) {
            notes = new String[]{};
        }

        String[] needToTalkIssues = paramMap.get("need_to_talk_issue_ids");
        if (needToTalkIssues == null) {
            needToTalkIssues = new String[]{};
        }

        String[] goingToFinishIssues = paramMap.get("going_to_finish_issue_ids");
        if (goingToFinishIssues == null) {
            goingToFinishIssues = new String[]{};
        }

        String[][] contentsValues = { notes, needToTalkIssues, goingToFinishIssues };

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        new ObjectOutputStream(out).writeObject(contentsValues);
        String contents = new String(Hex.encodeHex(out.toByteArray()));

        LocalDate localDate = LocalDate.now();
        Date date = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());

        userReport.setUserId(currentUser.getId());
        userReport.setContents(contents);
        userReport.setCreatedAt(date);
        userReport.setUpdatedAt(date);
        userReport.save();

        doGet(request, response);
    }

}
