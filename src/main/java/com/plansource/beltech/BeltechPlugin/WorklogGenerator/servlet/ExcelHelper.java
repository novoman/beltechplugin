package com.plansource.beltech.BeltechPlugin.WorklogGenerator.servlet;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.xssf.usermodel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.poi.ss.usermodel.Cell;

import java.util.ArrayList;
import java.util.HashMap;

public class ExcelHelper {
    final public static Short ROW_HEIGHT_BETWEEN_GROUPS = 12;
    final public static Short ROW_HEIGHT_BETWEEN_DAYS = 2;

    private static final Logger log = LoggerFactory.getLogger(WorklogGeneratorServlet.class);

    public static CellStyle redColorStyle;
    public static CellStyle defaultStyle;
    public static CellStyle decimalStyle;
    public static CellStyle boldStyle;

    public static CellStyle boldRightAlignmentStyle;
    public static XSSFCellStyle brownColorStyle;

    public static void setStyles(XSSFSheet sheet)
    {
        int COLUMN_0_WIDTH = 12;
        int COLUMN_1_WIDTH = 15;
        int COLUMN_2_WIDTH = 8;
        int COLUMN_3_WIDTH = 80;
        int COLUMN_4_WIDTH = 6;

        short DEFAULT_FONT_SIZE = 11;
        short RED_FONT_SIZE = 11;

        sheet.setColumnWidth(0, COLUMN_0_WIDTH *256);
        sheet.setColumnWidth(1, COLUMN_1_WIDTH *256);
        sheet.setColumnWidth(2, COLUMN_2_WIDTH *256);
        sheet.setColumnWidth(3, COLUMN_3_WIDTH*256);
        sheet.setColumnWidth(4, COLUMN_4_WIDTH*256);

        Font boldFont = sheet.getWorkbook().createFont();
        boldFont.setFontName("Arial");
        boldFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
        boldStyle = sheet.getWorkbook().createCellStyle();
        boldStyle.setFont(boldFont);

        boldRightAlignmentStyle = sheet.getWorkbook().createCellStyle();
        boldRightAlignmentStyle.setFont(boldFont);
        boldRightAlignmentStyle.setAlignment(CellStyle.ALIGN_RIGHT);
        boldRightAlignmentStyle.setTopBorderColor(Font.COLOR_RED);

        Font defaultFont = sheet.getWorkbook().createFont();

        defaultFont.setFontHeightInPoints(DEFAULT_FONT_SIZE);
        defaultFont.setFontName("Arial");
        defaultStyle = sheet.getWorkbook().createCellStyle();
        defaultStyle.setFont(defaultFont);

        DataFormat format = sheet.getWorkbook().createDataFormat();
        decimalStyle = sheet.getWorkbook().createCellStyle();
        decimalStyle.setDataFormat(format.getFormat("0.00"));
        decimalStyle.setFont(defaultFont);

        Font redFont = sheet.getWorkbook().createFont();
        redFont.setFontName("Arial");
        redFont.setColor(Font.COLOR_RED);
        redFont.setFontHeightInPoints(RED_FONT_SIZE);
        redFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
        redColorStyle = sheet.getWorkbook().createCellStyle();
        redColorStyle.setFont(redFont);

        brownColorStyle = sheet.getWorkbook().createCellStyle();
        brownColorStyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(162, 162, 162)));
        brownColorStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
    }

    public static XSSFWorkbook writeDataToExcel(ArrayList<HashMap<String, String>> allData, String dateRangeStart) {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet(dateRangeStart);

        setStyles(sheet);

        int row_number = 0;

        XSSFRow row = sheet.createRow(row_number++);

        writeCell(row,0, "Date", ExcelHelper.boldStyle);
        writeCell(row,1, "Developer", ExcelHelper.boldStyle);
        writeCell(row,2, "Issue #", ExcelHelper.boldStyle);
        writeCell(row,3, "Description", ExcelHelper.boldStyle);
        writeCell(row,4, "Hours", ExcelHelper.boldStyle);

        String current_group = allData.get(0).get("user_group");
        String current_date = allData.get(0).get("date");

        for (HashMap<String, String> userData : allData) {
            if (!current_date.equals(userData.get("date"))) {
                XSSFRow day_change_row = sheet.createRow(row_number++);
                for (int i = 0; i < 5; i++ ) {
                    Cell cell = day_change_row.createCell(i);
                    cell.setCellValue("");
                    cell.setCellStyle(brownColorStyle);
                }
                day_change_row.setRowStyle(brownColorStyle);
                day_change_row.setHeightInPoints(ExcelHelper.ROW_HEIGHT_BETWEEN_DAYS);
            }
            else {
                if (!current_group.equals(userData.get("user_group"))) {
                    XSSFRow empty_row = sheet.createRow(row_number++);
                    for (int i = 0; i < 5; i++ ) {  writeCell(empty_row, i, ""); }
                    empty_row.setHeightInPoints(ExcelHelper.ROW_HEIGHT_BETWEEN_GROUPS);
                }
            }

            XSSFRow tmp_row;
            tmp_row = sheet.createRow(row_number++);
            writeCell(tmp_row, 0, userData.get("date"));

            if ((userData.get("incomplete").equals("true") || (userData.get("hours").equals("-"))) && !userData.get("vacation").equals("true")) {
                writeCell(tmp_row, 1, userData.get("user_name"), ExcelHelper.redColorStyle);
            }
            else if (userData.get("vacation").equals("true")) {
                writeCell(tmp_row, 1, userData.get("user_name"), ExcelHelper.boldStyle);
            }
            else {
                writeCell(tmp_row, 1, userData.get("user_name"));
            }

            writeCell(tmp_row, 2, userData.get("issue_id"));
            if (userData.get("vacation").equals("true")) {
                writeCell(tmp_row, 3, userData.get("description"), ExcelHelper.boldStyle);
            }
            else {
                writeCell(tmp_row, 3, userData.get("description"));
            }

            XSSFCell cell = tmp_row.createCell(4);
            cell.setCellStyle(ExcelHelper.decimalStyle);

            if (userData.get("hours").equals("-")) {
                cell.setCellValue(userData.get("hours"));
                cell.setCellStyle(ExcelHelper.boldRightAlignmentStyle);
            }
            else {
                cell.setCellValue(Float.parseFloat(userData.get("hours")));
            }

            current_group = userData.get("user_group");
            current_date = userData.get("date");
        }

        sheet.createRow(row_number++);
        XSSFRow result_row = sheet.createRow(row_number++);
        writeCell(result_row,3, "Total Hours:", ExcelHelper.boldStyle);
        String strFormula= "SUM(E2:E" + (row_number - 2) + ")";

        XSSFCell result_cell = result_row.createCell(4);
        result_cell.setCellType(XSSFCell.CELL_TYPE_FORMULA);
        result_cell.setCellFormula(strFormula);
        result_cell.setCellStyle(ExcelHelper.boldStyle);

        return workbook;
    }

    private static void writeCell(XSSFRow row, int index, String text, CellStyle style)
    {
        XSSFCell cell = row.createCell(index);
        cell.setCellValue(text);
        cell.setCellStyle(style);
    }

    private static void writeCell(XSSFRow row, int index, String text)
    {
        writeCell(row, index, text, ExcelHelper.defaultStyle);
    }
}
