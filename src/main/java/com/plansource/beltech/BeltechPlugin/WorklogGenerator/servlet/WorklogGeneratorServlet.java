package com.plansource.beltech.BeltechPlugin.WorklogGenerator.servlet;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.velocity.VelocityManager;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.xssf.usermodel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Scanned
public class WorklogGeneratorServlet extends HttpServlet
{
    private static final Logger log = LoggerFactory.getLogger(WorklogGeneratorServlet.class);

    private VelocityManager velocityManager;
    private PageBuilderService pageBuilderService;
    private ActiveObjects activeObjects;

    public WorklogGeneratorServlet(@ComponentImport VelocityManager velocityManager, @ComponentImport PageBuilderService pageBuilderService, @ComponentImport ActiveObjects activeObjects)
    {
        this.velocityManager = velocityManager;
        this.pageBuilderService = pageBuilderService;
        this.activeObjects = activeObjects;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException
    {
        this.pageBuilderService
            .assembler()
            .resources()
            .requireWebResource("com.plansource.beltech.BeltechPlugin:jquery-ui-resources")
            .requireWebResource("com.plansource.beltech.BeltechPlugin:worklog-generator-resources");

        response.setContentType("text/html");

        Map<String, Object> context = Maps.newHashMap();
        LocalDate localDate = LocalDate.now();
        LocalDate from;
        LocalDate to;

        if (isWeekendDay(localDate)) {
            from = localDate.with(TemporalAdjusters.previous(DayOfWeek.MONDAY));
            to = localDate.with(TemporalAdjusters.nextOrSame(DayOfWeek.SUNDAY));
        }
        else {
            LocalDate prevSaturday = localDate.with(TemporalAdjusters.previous(DayOfWeek.SATURDAY));
            from = prevSaturday.with(TemporalAdjusters.previous(DayOfWeek.MONDAY));
            to = localDate.with(TemporalAdjusters.previous(DayOfWeek.SUNDAY));
        }

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("YYYY-MM-dd");
        context.put("dateFrom", formatter.format(from));
        context.put("dateTo", formatter.format(to));

        for (Map<String, String> curr_group : DataGenerator.BELTECH_USER_GROUPS)
        {
            for (Map.Entry<String, String> entry : curr_group.entrySet())
            {
                String code = entry.getKey();
                String group = entry.getValue();

                try {
                    context.put("users_" + code, DataGenerator.getUsersCollection(group));
                } catch (Exception e) {
                    response.getWriter().write("There are no Users in " + group + " group");
                    response.getWriter().close();
                }
            }
        }

        String content = this.velocityManager.getEncodedBody("/templates/", "worklog.vm", "UTF-8", context);
        response.getWriter().write(content);
        response.getWriter().close();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    {
        String dateRangeStart = request.getParameter("start-date");
        String dateRangeEnd = request.getParameter("end-date");

        LocalDate date_from = LocalDate.parse(dateRangeStart);
        LocalDate date_to = LocalDate.parse(dateRangeEnd);

        if (date_from.isAfter(date_to)) {
            return;
        }

        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment; filename=PlanSource Timesheet " + dateRangeStart + " - " + dateRangeEnd + ".xlsx");

        DataGenerator data = new DataGenerator(activeObjects);
        ArrayList<HashMap<String, String>> allData = data.generateData(date_from, date_to);

        XSSFWorkbook workbook = ExcelHelper.writeDataToExcel(allData, dateRangeStart);

        try {
            workbook.write(response.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }

        allData.clear();
    }

    private boolean isWeekendDay(LocalDate date)
    {
        String[] weekend = {"SATURDAY", "SUNDAY"};
        int pos = StringUtils.indexOfAny(date.getDayOfWeek().name(), weekend);

        return pos != -1;
    }
}
