package com.plansource.beltech.BeltechPlugin.WorklogGenerator.servlet;

import com.atlassian.jira.user.ApplicationUser;
import java.util.Comparator;
import java.util.List;

public class SortUsers {

    public static List<ApplicationUser> sortByDisplayName(List<ApplicationUser> users)
    {
        Comparator<ApplicationUser> compareByDisplayName = Comparator.comparing(ApplicationUser::getDisplayName);
        users.sort(compareByDisplayName);

        return users;
    }

    public static List<ApplicationUser> sortByLastName(List<ApplicationUser> users)
    {
        boolean sorted = false;
        ApplicationUser temp;

        while (!sorted) {
            sorted = true;
            for (int i = 0; i < users.size() - 1; i++) {
                if (getLastName(users.get(i).getDisplayName()).compareTo(getLastName(users.get(i + 1).getDisplayName())) > 0) {
                    temp = users.get(i);
                    users.set(i, users.get(i + 1));
                    users.set(i + 1, temp);
                    sorted = false;
                }
            }
        }

        return users;
    }

    private static String getLastName(String fullName)
    {
        String[] splitName = fullName.split(" ");

        return splitName.length > 1 ? splitName[1] : splitName[0];
    }

}
