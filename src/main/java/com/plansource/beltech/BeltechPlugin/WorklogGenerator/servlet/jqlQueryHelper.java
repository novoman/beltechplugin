package com.plansource.beltech.BeltechPlugin.WorklogGenerator.servlet;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.jql.parser.JqlParseException;
import com.atlassian.jira.jql.parser.JqlQueryParser;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.query.Query;
import com.plansource.beltech.BeltechPlugin.Vacations.ao.UserRequests;
import org.apache.commons.lang3.ArrayUtils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class jqlQueryHelper {

    private JqlQueryParser jqlQueryParser;
    private SearchService searchService;

    public jqlQueryHelper() {
        this.jqlQueryParser = ComponentAccessor.getComponent(JqlQueryParser.class);
        this.searchService = ComponentAccessor.getComponent(SearchService.class);
    }

    public SearchResults<Issue> getIssuesByDateAndWorklogAuthor(LocalDate currDate, ApplicationUser user)
    {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");

        String theQuery = "(worklogDate = \"" + formatter.format(currDate) + "\" OR worklogDate = \"" + formatter.format(currDate.plusDays(1)) + "\")";
        theQuery = theQuery + " AND worklogAuthor = " + user.getUsername();

        SearchResults<Issue> results = null;

        try {
            Query query = this.jqlQueryParser.parseQuery(theQuery);
            results = this.searchService.search(ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser(), query, PagerFilter.getUnlimitedFilter());
        }
        catch (JqlParseException | SearchException e) {
            e.printStackTrace();
        }

        return results;
    }

    public List<Issue> getActiveIssuesForUser(ApplicationUser user)
    {
        String theQuery = "assignee = " + user.getUsername() + " ORDER BY priority DESC";

        SearchResults<Issue> results = null;

        try {
            Query query = this.jqlQueryParser.parseQuery(theQuery);
            results = this.searchService.search(ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser(), query, PagerFilter.getUnlimitedFilter());
        }
        catch (JqlParseException | SearchException e) {
            e.printStackTrace();
        }

        List<Issue> issues = new ArrayList<>();

        if (results != null) {
            issues = results.getResults();
        }

        return issues;
    }

    public List<Issue> getActiveSharedIssuesByIds(String issuesIds)
    {
        List<Issue> issues = new ArrayList<>();

        if (!"".equals(issuesIds)) {
            String theQuery = "id IN (" + issuesIds + ") ORDER BY priority DESC";

            SearchResults<Issue> results = null;

            try {
                Query query = this.jqlQueryParser.parseQuery(theQuery);
                results = this.searchService.search(ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser(), query, PagerFilter.getUnlimitedFilter());
            }
            catch (JqlParseException | SearchException e) {
                e.printStackTrace();
            }

            if (results != null) {
                issues = results.getResults();
            }
        }

        return issues;
    }

    public List<Issue> getActivityIssuesForTodayForUser(ApplicationUser user)
    {
        String theQuery = "(assignee changed by " + user.getUsername() + " during (startOfDay(), endOfDay())) OR (worklogDate >= startOfDay() AND worklogDate <= endOfDay() AND worklogAuthor = " + user.getUsername() + ") ORDER BY priority DESC";

        SearchResults<Issue> results = null;

        try {
            Query query = this.jqlQueryParser.parseQuery(theQuery);
            results = this.searchService.search(ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser(), query, PagerFilter.getUnlimitedFilter());
        }
        catch (JqlParseException | SearchException e) {
            e.printStackTrace();
        }

        List<Issue> issues = new ArrayList<>();

        if (results != null) {
            issues = results.getResults();
        }

        return issues;
    }

    public Issue getIssueByKey(String key)
    {
        String theQuery = "id = " + key;

        SearchResults<Issue> results = null;

        try {
            Query query = this.jqlQueryParser.parseQuery(theQuery);
            results = this.searchService.search(ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser(), query, PagerFilter.getUnlimitedFilter());
        }
        catch (JqlParseException | SearchException e) {
            e.printStackTrace();
        }

        List<Issue> issues;

        if (results != null) {
            issues = results.getResults();
            if (issues.size() > 0) {
                return issues.get(0);
            }
        }

        return null;
    }
}
