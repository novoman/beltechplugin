package com.plansource.beltech.BeltechPlugin.WorklogGenerator.servlet;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.issue.worklog.Worklog;
import com.atlassian.jira.issue.worklog.WorklogManager;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.user.ApplicationUser;
import com.plansource.beltech.BeltechPlugin.Vacations.ao.UserRequests;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class DataGenerator {
    private static final Logger log = LoggerFactory.getLogger(DataGenerator.class);

    final double WORKDAY_LENGTH = 8.0;
    private WorklogManager worklogManager;
    private ActiveObjects activeObjects;

    private static GroupManager groupManager = ComponentAccessor.getComponent(GroupManager.class);

    public ArrayList<HashMap<String, String>> allData = new ArrayList<>();

    final public static ArrayList<Map<String, String>> BELTECH_USER_GROUPS = new ArrayList<Map<String, String>>(){{
        add(new HashMap<String, String>(){{
            put("ipu", "BelTech Belarus IPUs");
        }} );
        add(new HashMap<String, String>(){{
            put("slc", "BelTech SLC");
        }} );
        add(new HashMap<String, String>(){{
            put("ch", "BelTech Belarus CH");
        }} );
        add(new HashMap<String, String>(){{
            put("qa", "BelTech QA");
        }} );
    }};

    public DataGenerator(ActiveObjects activeObjects)
    {
        this.activeObjects = activeObjects;
        this.worklogManager = ComponentAccessor.getComponent(WorklogManager.class);
    }

    public ArrayList<HashMap<String, String>> generateDataForTimesheetValidator(LocalDate curr_date, LocalDate date_to)
    {
        String group = "BelTech Belarus IPUs";

        while (!curr_date.isEqual(date_to.plusDays(1))) {
            for (ApplicationUser user : SortUsers.sortByLastName(getUsersCollection(group))) {
                this.allData.addAll(generateUserData(user, curr_date, group));
            }
            curr_date = curr_date.plusDays(1);
        }

        return allData;
    }

    public ArrayList<HashMap<String, String>> generateData(LocalDate curr_date, LocalDate date_to)
    {
        while (!curr_date.isEqual(date_to.plusDays(1)))
        {
            for (Map<String, String> curr_group : BELTECH_USER_GROUPS)
            {
                for (Map.Entry<String, String> entry : curr_group.entrySet())
                {
                    String group = entry.getValue();

                    for (ApplicationUser user : SortUsers.sortByLastName(getUsersCollection(group)))
                    {
                        this.allData.addAll(generateUserData(user, curr_date, group));
                    }
                }
            }
            curr_date = curr_date.plusDays(1);
        }

        return allData;
    }

    private ArrayList<HashMap<String, String>> generateUserData(ApplicationUser user, LocalDate curr_date, String group)
    {
        jqlQueryHelper jql = new jqlQueryHelper();
        SearchResults<Issue> results = jql.getIssuesByDateAndWorklogAuthor(curr_date, user);

        List<Issue> issues = new ArrayList<>();

        if (results != null) {
            issues = results.getResults();
        }

        return generateDataForUser(issues, curr_date, user, group);
    }

    public static List<ApplicationUser> getUsersCollection(String group)
    {
        Collection<ApplicationUser> Collection = groupManager.getUsersInGroup(groupManager.getGroup(group));
        Collection.removeIf(user -> (!user.isActive() || user.getDisplayName().equals("Igor Motro")));

        return new ArrayList<>(Collection);
    }

    private ArrayList<HashMap<String, String>> generateDataForUser(List<Issue> issues, LocalDate curr_date, ApplicationUser user, String group) {
        double userHoursLogged = 0.0;
        ArrayList<HashMap<String, String>> userData = new ArrayList<>();
        List<Integer> user_row_numbers = new ArrayList<>();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/YYYY");

        for (Issue issue : issues)
        {
            for (Worklog worklog: this.worklogManager.getByIssue(issue))
            {
                if (dateAndLocalTimeCompare(worklog.getStartDate(), curr_date, group) && (user.getKey().equals(worklog.getAuthorKey())))
                {
                    userData.add(new HashMap<String, String>(){{
                        put("user_key", user.getKey());
                        put("user_group", group);
                        put("date", formatter.format(curr_date));
                        put("user_name", worklog.getAuthorObject().getDisplayName());
                        put("issue_id", issue.getKey());
                        put("description", issue.getSummary());
                        put("hours", getTimeInHour(worklog.getTimeSpent()));
                        put("incomplete", "false");
                        put("vacation", "false");
                    }});

                    userHoursLogged = userHoursLogged + Double.parseDouble(getTimeInHour(worklog.getTimeSpent()));
                    user_row_numbers.add(userData.size() - 1);
                }
            }
        }

        if (userHoursLogged != this.WORKDAY_LENGTH)
        {
            if (userHoursLogged != 0.0)
            {
                for (Integer user_row_number : user_row_numbers)
                {
                    userData.get(user_row_number).put("incomplete", "true");
                }
            }

            if (userHoursLogged == 0.0 && !isHoliday(curr_date))
            {
                UserRequests[] userRequests = getUserRequestReportOnDate(user, curr_date);

                if (userRequests.length > 0) {
                    UserRequests userRequest = userRequests[0];
                    userData.add(new HashMap<String, String>(){{
                        put("user_key", user.getKey());
                        put("user_group", group);
                        put("date", formatter.format(curr_date));
                        put("user_name", user.getDisplayName());
                        put("issue_id", "");
                        put("description", getUserRequestDescription(userRequest));
                        put("hours", "-");
                        put("incomplete", "false");
                        put("vacation", "true");
                    }});
                }
                else {
                    userData.add(new HashMap<String, String>(){{
                        put("user_key", user.getKey());
                        put("user_group", group);
                        put("date", formatter.format(curr_date));
                        put("user_name", user.getDisplayName());
                        put("hours", "-");
                        put("incomplete", "true");
                        put("vacation", "false");
                    }});
                }
            }
        }

        return userData;
    }

    private String getUserRequestDescription(UserRequests userRequest)
    {
        String result = "";

        if (userRequest == null) {
            return result;
        }

        switch (userRequest.getRequestType()) {
            case working_day_transfer:
                result += "Day off";

                LocalDate date = LocalDate.parse(userRequest.getEndDate().toString().substring(0, 10));
                if (date != null) {
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/YYYY");
                    result += " (worked on " + formatter.format(date) + ")";
                }
                break;
            case day_off:
                result += "Day off";
                break;
            case sick_day:
                result += "Was sick";
                break;
            case vacation:
                result += "Vacation";
                break;
        }

        return result;
    }

    private UserRequests[] getUserRequestReportOnDate(ApplicationUser user, LocalDate curr_date)
    {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        UserRequests[] tempUserRequests, userRequests = new UserRequests[0];

        tempUserRequests = this.activeObjects.find(
                UserRequests.class,
                "START_DATE <= ? AND END_DATE >= ? AND APPROVE_RESULT = ? AND REQUEST_BY_ID = ? AND (REQUEST_TYPE = ? OR REQUEST_TYPE = ? OR REQUEST_TYPE = ?)",
                formatter.format(curr_date),
                formatter.format(curr_date),
                UserRequests.RequestResults.approved,
                user.getId(),
                UserRequests.RequestTypes.vacation,
                UserRequests.RequestTypes.day_off,
                UserRequests.RequestTypes.sick_day
        );
        userRequests = ArrayUtils.addAll(userRequests, tempUserRequests);

        tempUserRequests = this.activeObjects.find(UserRequests.class, "START_DATE = ? AND REQUEST_TYPE = ? AND APPROVE_RESULT = ? AND REQUEST_BY_ID = ?", formatter.format(curr_date), UserRequests.RequestTypes.working_day_transfer, UserRequests.RequestResults.approved, user.getId());
        userRequests = ArrayUtils.addAll(userRequests, tempUserRequests);

        return userRequests;
    }

    private Boolean dateAndLocalTimeCompare(Date date, LocalDate localDate, String group)
    {
        Instant instant = Instant.ofEpochMilli(date.getTime());
        LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, getTimeZoneForGroup(group));
        LocalDate localDate1 = localDateTime.toLocalDate();

        return localDate1.isEqual(localDate);
    }

    private ZoneId getTimeZoneForGroup(String group)
    {
        ZoneId zoneId;

        switch (group) {
            case "BelTech Belarus IPUs":
            case "BelTech Belarus CH":
            case "BelTech QA":
                zoneId = ZoneId.of("Europe/Minsk");     // UTC+03:00
                break;
            case "BelTech SLC":
                zoneId = ZoneId.of("US/Central");   // UTC-05:00
                break;
            default:
                zoneId = ZoneId.systemDefault();

        }

        return zoneId;
    }

    private String getTimeInHour(Long timeInSeconds)
    {
        double hours = timeInSeconds / 3600.0;

        return Double.toString(hours);
    }

    private boolean isHoliday(LocalDate date)
    {
        String[] holidays = {"SATURDAY", "SUNDAY"};
        int pos = StringUtils.indexOfAny(date.getDayOfWeek().name(), holidays);

        return pos != -1;
    }
}
