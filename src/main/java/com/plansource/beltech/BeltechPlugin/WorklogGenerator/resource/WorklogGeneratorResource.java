package com.plansource.beltech.BeltechPlugin.WorklogGenerator.resource;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.plansource.beltech.BeltechPlugin.WorklogGenerator.servlet.DataGenerator;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;

@Path("/timesheet")
public class WorklogGeneratorResource
{
	private ActiveObjects activeObjects;

	public WorklogGeneratorResource(ActiveObjects activeObjects)
	{
		this.activeObjects = activeObjects;
	}

	@GET
	@Path("/generate")
	@Produces({MediaType.APPLICATION_JSON})
	public Response generate(@Context HttpServletRequest request, @QueryParam("start-date") String startDate, @QueryParam("end-date") String endDate)
	{
		if (startDate == null || endDate == null || "".equals(startDate) || "".equals(endDate)) {
			return Response.serverError().entity("Please specify 'start-date' and 'end-date' in request's params.").build();
		}

		try {
			LocalDate date_from = LocalDate.parse(startDate);
			LocalDate date_to = LocalDate.parse(endDate);

			if (date_from.isAfter(date_to)) {
				return Response.serverError().entity("'start-date' can't be after 'end-date'.").build();
			}

			DataGenerator data = new DataGenerator(activeObjects);
			ArrayList<HashMap<String, String>> allData = data.generateData(date_from, date_to);

			return Response.ok(allData).build();
		}
		catch (Exception e) {
			return Response.serverError().entity(e.toString()).build();
		}
	}
}
