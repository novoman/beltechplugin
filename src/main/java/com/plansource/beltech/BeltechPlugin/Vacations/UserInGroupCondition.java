package com.plansource.beltech.BeltechPlugin.Vacations;

import com.atlassian.jira.plugin.webfragment.conditions.AbstractWebCondition;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.PluginParseException;

import java.util.Map;

public class UserInGroupCondition extends AbstractWebCondition {
    private String groupName;

    public UserInGroupCondition() {
    }

    public void init(Map<String,String> params) throws PluginParseException
    {
        this.groupName = params.get("groupName");
        super.init(params);
    }

    public boolean shouldDisplay(ApplicationUser user, JiraHelper jiraHelper) {
        String userRole = VacationsHelper.getCurrentUserRole().toLowerCase();

        return this.groupName.equals(VacationsHelper.beltechUserRoles.get(userRole));
    }
}
