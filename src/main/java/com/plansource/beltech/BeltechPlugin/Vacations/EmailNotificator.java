package com.plansource.beltech.BeltechPlugin.Vacations;
import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.mail.Email;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.mail.queue.SingleMailQueueItem;
import com.plansource.beltech.BeltechPlugin.Vacations.ao.UserRequests;

import java.util.HashMap;
import java.util.Map;

public class EmailNotificator
{
    private UserManager userManager;
    private VacationsHelper vacationsHelper;

    private static final String beltechVacationPath = "https://jira.plansource.com/secure/Vacations.jspa";

    private static final Map<String, String> beltechEmails = new HashMap<String, String>()
    {
        {
//        put("BELTECH_PM", "enaruta@beltechsoftware.com, mshuleyko@beltechsoftware.com, smazur@beltechsoftware.com, dkadyrko@plansource.com, akushnir@plansource.com");
          put("BELTECH_BOSS", "igor.motro@834andmore.com, vtabachenko@beltechsoftware.com");
        };
    };

    public EmailNotificator(ActiveObjects activeObjects, UserManager userManager) {
        this.userManager = userManager;
        this.vacationsHelper = new VacationsHelper(activeObjects, this.userManager);
        beltechEmails.put("BELTECH_PM", this.vacationsHelper.beltechPmEmails());
    }

    public boolean sendRequestedNotificationEmail(UserRequests request)
    {
        if (request.getRequestById() == null) {
            return false;
        }

        String to, from_account, from, userName, subject, take, message;

        if (this.userManager.getUserById(request.getRequestById()).isPresent()) {
            ApplicationUser user = this.userManager.getUserById(request.getRequestById()).get();

            from_account = user.getUsername().isEmpty() ? "jira" : user.getUsername();
            from = from_account + "@jira.plansource.com";
            to = beltechEmails.get("BELTECH_PM") + (user.getEmailAddress().isEmpty() ? "" : ", " + user.getEmailAddress());
            userName = user.getDisplayName().isEmpty() ? "unknown" : user.getDisplayName();
            subject = "Request for " + this.vacationsHelper.requestToString(request, VacationsHelper.withoutApproveInf) + " from " + userName;
            take = request.getRequestType().equals(UserRequests.RequestTypes.cancel) ? "" : " take";

            message = "Hello BelTech PMs,\nI would like to" + take + " " + this.vacationsHelper.requestToString(request, VacationsHelper.withoutApproveInf) + ". ";
            message += "Please approve " + beltechVacationPath + "\n\n";
            if (request.getDescription() != null) {
                message += request.getDescription() + "\n\n";
            }
            message += "Notice: This message was automatically generated. Please don’t reply on it.\n\n";
            message += "Thank you, \n" + userName;

            return sendMail(to, from, subject, message);
        }

        return false;
    }

    public boolean sendProcessedNotificationEmail(UserRequests request, String deletedRequestToString)
    {
        if (request.getRequestById() == null) {
            return false;
        }

        String to, from_account, from, userName, processedBy, processedResult, subject, take, message, requestToString;

        if (this.userManager.getUserById(request.getRequestById()).isPresent()) {
            ApplicationUser user = this.userManager.getUserById(request.getRequestById()).get();

            from_account = user.getUsername().isEmpty() ? "jira" : user.getUsername();
            from = from_account + "@jira.plansource.com";
            to = beltechEmails.get("BELTECH_PM") + ", " + beltechEmails.get("BELTECH_BOSS") + (user.getEmailAddress().isEmpty() ? "" : ", " + user.getEmailAddress());
            userName = user.getDisplayName().isEmpty() ? "unknown" : user.getDisplayName();
            processedResult = (request.getApproveResult() == null) || request.getApproveResult().equals(UserRequests.RequestResults.approved) ? "Approve" : "Decline";
            requestToString = deletedRequestToString.isEmpty() ? this.vacationsHelper.requestToString(request, VacationsHelper.withoutApproveInf) : deletedRequestToString;

            subject = processedResult + " for " + requestToString + " from " + userName;
            take = request.getRequestType().equals(UserRequests.RequestTypes.cancel) ? "" : " take";

            if ((request.getApproveResult() == null) || request.getApproveResult().equals(UserRequests.RequestResults.approved)) {
                message = "Hi, " + userName + ",\n\nWe are Ok with this. User event was added to your account.\n\n";
            }
            else {
                message = "Hi, " + userName + ",\n\nRequest declined.\n\n";
            }

            if (request.getPmDescription() != null) {
                message += request.getPmDescription() + "\n\n";
            }

            processedBy = "";

            if (this.userManager.getUserById(request.getApprovedById()).isPresent()) {
                if (!this.userManager.getUserById(request.getApprovedById()).get().getDisplayName().isEmpty()) {
                    processedBy = this.userManager.getUserById(request.getApprovedById()).get().getDisplayName();
                }
            }
            message += "Thank you, \n" + processedBy + "\n\n\n";

            message += "Hello BelTech PMs,\nI would like to" + take + " " + requestToString + ". ";
            message += "Please approve " + beltechVacationPath + "\n\n";
            if (request.getDescription() != null) {
                message += request.getDescription() + "\n\n";
            }
            message += "Notice: This message was automatically generated. Please don’t reply on it.\n\n";
            message += "Thank you, \n" + userName;

            return sendMail(to, from, subject, message);
        }

        return false;
    }

    public boolean sendAutoApprovedNotificationEmail(UserRequests request, String deletedRequestToString)
    {
        if (request.getRequestById() == null) {
            return false;
        }

        String to, from_account, from, userName, subject, take, message, requestToString;

        if (this.userManager.getUserById(request.getRequestById()).isPresent()) {
            ApplicationUser user = this.userManager.getUserById(request.getRequestById()).get();

            // do not send e-mail if Beltech PM
            if ("PM".equals(VacationsHelper.getCurrentUserRole())) {
                return true;
            }

            from_account = user.getUsername().isEmpty() ? "jira" : user.getUsername();
            from = from_account + "@jira.plansource.com";
            to = beltechEmails.get("BELTECH_BOSS") + (user.getEmailAddress().isEmpty() ? "" : ", " + user.getEmailAddress());
            userName = user.getDisplayName().isEmpty() ? "unknown" : user.getDisplayName();

            requestToString = deletedRequestToString.isEmpty() ? this.vacationsHelper.requestToString(request, VacationsHelper.withoutApproveInf) : deletedRequestToString;

            subject = "Approve for " + requestToString + " from " + userName;
            take = request.getRequestType().equals(UserRequests.RequestTypes.cancel) ? "" : " take";

            message = "Hi, " + userName + ",\n\nWe are Ok with this. User event was added to your account.\n\n";
            message += "Thank you, \n\n";

            message += "I would like to" + take + " " + requestToString + ".\n\n";
            if (request.getDescription() != null) {
                message += request.getDescription() + "\n\n";
            }
            message += "Notice: This message was automatically generated. Please don’t reply on it.\n\n";
            message += "Thank you, \n" + userName;

            return sendMail(to, from, subject, message);
        }

        return false;
    }

    private boolean sendMail(String to, String from, String subject, String message)
    {
        try {
            Email email = new Email(to);
            email.setFrom(from);
            email.setSubject(subject);
            email.setBody(message);
            email.setMimeType("text/plain");

            SingleMailQueueItem smqi = new SingleMailQueueItem(email);
            ComponentAccessor.getMailQueue().addItem(smqi);

            return true;
        }
        catch (Exception e) {
            return false;
        }
    }
}
