package com.plansource.beltech.BeltechPlugin.Vacations;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Stream;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import net.java.ao.Query;
import com.plansource.beltech.BeltechPlugin.Vacations.ao.UserRequests;
import org.apache.commons.lang3.ArrayUtils;

public class VacationsHelper {

    private ActiveObjects activeObjects;
    private UserManager userManager;
    private Map<String, Collection<ApplicationUser>> applicationUserList;
    private UserRequests[] allUserRequests;

    public static final String withoutApproveInf = "without_approve_inf";

    public static final Map<String, String> beltechUserRoles = new HashMap<String, String>()
    {
        {
            put("dev", "BelTech Belarus IPUs");
            put("pm", "BelTech SLC");
            put("ch", "BelTech Belarus CH");
            put("qa", "BelTech QA");
        };
    };

    public VacationsHelper(ActiveObjects activeObjects, UserManager userManager)
    {
        this.activeObjects = activeObjects;
        this.userManager = userManager;
        this.applicationUserList = getAllBeltechUsers();
        this.allUserRequests = getAllUserRequests();
    }

    public boolean isValidRequestType(String type)
    {
        for (UserRequests.RequestTypes rt : UserRequests.RequestTypes.values()) {
            if (rt.name().equals(type)) {
                return true;
            }
        }

        return false;
    }

    public boolean isValidDatesForRequest(UserRequests request)
    {
        if (request.getStartDate().after(request.getEndDate()) && !request.getRequestType().equals(UserRequests.RequestTypes.working_day_transfer)) {
            return false;
        }
        else if (request.getEndDate().after(request.getStartDate()) && request.getRequestType().equals(UserRequests.RequestTypes.working_day_transfer)) {
            return false;
        }

        return true;
    }

    public String getMonthName(int monthNumber) {
        String[] months = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
        return months[monthNumber - 1];
    }

    public int getCurrentYear() {
        return Calendar.getInstance().get(Calendar.YEAR);
    }

    public String getIntersectionCalendarCellClasses(String dateString, String paramsUserId, String countForClass)
    {
        String classes = "";
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        sdf.setLenient(false);

        try {
            Date date = sdf.parse(dateString);

            if (date != null) {
                classes += "cell";
                SimpleDateFormat simpleDateformat = new SimpleDateFormat("EEEE");
                if (simpleDateformat.format(date).equals("Saturday") || simpleDateformat.format(date).equals("Sunday")) {
                    classes += " weekend";
                }
                else {
                    int summaryLevelNumber = Integer.parseInt(countForClass);
                    if (summaryLevelNumber > 9) summaryLevelNumber = 9;

                    classes += " level-" + summaryLevelNumber;
                    Date today = sdf.parse(sdf.format(new Date()));

                    if (date.equals(today)) {
                        classes += " today";
                    }
                    else if (date.before(today)) {
                        classes += " past";
                    }
                }
            }
            else {
                classes = "date-invalid";
            }
        } catch (ParseException e) {
            return "date-invalid";
        }

        return classes;
    }

    public List<String> getProgrammersList(String dateString, String paramsUserId)
    {
        StringBuilder result = new StringBuilder();
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        try {
            Date date = sdf.parse(dateString);

            if (date != null) {
                UserRequests[] userRequests = findUserRequestsForCalendar(date, paramsUserId);

                for (UserRequests userRequest : userRequests) {
                    if (this.userManager.getUserById(userRequest.getRequestById()).isPresent()) {
                        result.append(this.userManager.getUserById(userRequest.getRequestById()).get().getDisplayName()).append("\n");
                    }
                }

                result.append("----------\nTOTAL ").append(userRequests.length);

                return Arrays.asList(result.toString(), String.valueOf(userRequests.length));
            }
        }
        catch (ParseException e) {
            return Arrays.asList("Parse error", "0");
        }

        return Arrays.asList("", "0");
    }

    private UserRequests[] getAllUserRequests()
    {
        return this.activeObjects.find(UserRequests.class, Query.select().order("START_DATE ASC"));
    }

    private UserRequests[] findUserRequestsForCalendar(Date date, String paramsUserId)
    {
        UserRequests[] userRequests = new UserRequests[0];
        List<ApplicationUser> users =  selectForUsersVacation("all", paramsUserId);

        for (ApplicationUser user : users) {
            userRequests = ArrayUtils.addAll(userRequests, Stream.of(this.allUserRequests).filter((ur) -> (ur.getStartDate() != null && ur.getStartDate().compareTo(date) <= 0) && (ur.getEndDate() != null && ur.getEndDate().compareTo(date) >= 0) && UserRequests.RequestResults.approved.equals(ur.getApproveResult()) && user.getId().equals(ur.getRequestById())).toArray(UserRequests[]::new));
            userRequests = ArrayUtils.addAll(userRequests, Stream.of(this.allUserRequests).filter((ur) -> (ur.getStartDate() != null && ur.getStartDate().compareTo(date) == 0) && UserRequests.RequestTypes.working_day_transfer.equals(ur.getRequestType()) && UserRequests.RequestResults.approved.equals(ur.getApproveResult()) && user.getId().equals(ur.getRequestById())).toArray(UserRequests[]::new));
        }

        return userRequests;
    }

    public String beltechPmEmails()
    {
        StringBuilder result = new StringBuilder();
        List<ApplicationUser> users = new ArrayList<>(this.applicationUserList.get(VacationsHelper.beltechUserRoles.get("pm")));

        for (ApplicationUser user : users) {
            result.append(user.getEmailAddress()).append(", ");
        }

        return result.substring(0, result.length() - 2);
    }

    private Map<String, Collection<ApplicationUser>> getAllBeltechUsers()
    {
        GroupManager groupManager = ComponentAccessor.getGroupManager();
        Map<String, Collection<ApplicationUser>> applicationUsers = new HashMap<>();

        for (String role : VacationsHelper.beltechUserRoles.values()) {
            Collection<ApplicationUser> users = groupManager.getUsersInGroup(role);
            users.removeIf(user -> (!user.isActive() || user.getDisplayName().equals("Igor Motro")));
            applicationUsers.put(role, users);
        }

        return applicationUsers;
    }

    public List<ApplicationUser> selectForUsersVacation(String typeForPm, String paramsUserId)
    {
        List<ApplicationUser> applicationUsers = new ArrayList<>();
        String userRole = getCurrentUserRole();

        if (!paramsUserId.equals("all")) {
            Long requestId = Long.parseLong(paramsUserId);
            if (this.userManager.getUserById(requestId).isPresent()) {
                applicationUsers.add(this.userManager.getUserById(requestId).get());
                return applicationUsers;
            }
        }

        switch (userRole) {
            case "PM":
                switch (typeForPm) {
                    case "all":
                        applicationUsers.addAll(this.applicationUserList.get(VacationsHelper.beltechUserRoles.get("pm")));
                        applicationUsers.addAll(this.applicationUserList.get(VacationsHelper.beltechUserRoles.get("dev")));
                        break;
                    case "pms":
                        applicationUsers.addAll(this.applicationUserList.get(VacationsHelper.beltechUserRoles.get("pm")));
                        break;
                    case "devs":
                        applicationUsers.addAll(this.applicationUserList.get(VacationsHelper.beltechUserRoles.get("dev")));
                        break;
                }
                break;
            case "CH":
                applicationUsers.addAll(this.applicationUserList.get(VacationsHelper.beltechUserRoles.get("ch")));
                break;
            case "QA":
                applicationUsers.addAll(this.applicationUserList.get(VacationsHelper.beltechUserRoles.get("qa")));
                break;
            case "DEV":
                applicationUsers.addAll(this.applicationUserList.get(VacationsHelper.beltechUserRoles.get("dev")));
                break;
        }

        return applicationUsers;
    }

    public static String getCurrentUserRole()
    {
        String userRole = "";

        ApplicationUser currentUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
        GroupManager groupManager = ComponentAccessor.getGroupManager();

        if (groupManager.isUserInGroup(currentUser, VacationsHelper.beltechUserRoles.get("pm")))  userRole = "PM";
        else if (groupManager.isUserInGroup(currentUser, VacationsHelper.beltechUserRoles.get("ch"))) userRole = "CH";
        else if (groupManager.isUserInGroup(currentUser, VacationsHelper.beltechUserRoles.get("qa"))) userRole = "QA";
        else if (groupManager.isUserInGroup(currentUser, VacationsHelper.beltechUserRoles.get("dev"))) userRole = "DEV";

        return userRole;
    }

    public UserRequests[] getUserRequestsForMyRequests(ApplicationUser user, String type, boolean isBeltechPm)
    {
        UserRequests[] userRequests;

        if ("submitted".equals(type)) {
            if (isBeltechPm) {
                userRequests = Stream.of(this.allUserRequests).filter((ur) -> (ur.getRequestById() != null && ur.getApproveResult() == null)).toArray(UserRequests[]::new);
            }
            else {
                userRequests = Stream.of(this.allUserRequests).filter((ur) -> user.getId().equals(ur.getRequestById()) && ur.getApproveResult() == null).toArray(UserRequests[]::new);
            }

        }
        else {
            userRequests = Stream.of(this.allUserRequests).filter((ur) -> user.getId().equals(ur.getRequestById()) && UserRequests.RequestResults.valueOf(type).equals(ur.getApproveResult())).toArray(UserRequests[]::new);
        }

        return userRequests;
    }

    public UserRequests[] getUserRequestsForProcessedVacations(ApplicationUser user, int year)
    {
        UserRequests[] userRequests = new UserRequests[0];
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        try {
            Date yearStart = sdf.parse(year + "-01-01");
            Date yearEnd = sdf.parse(year + "-12-31");

            if (yearStart != null && yearEnd != null) {
                UserRequests[] approvedProcessedRequests, declinedProcessedRequests;

                approvedProcessedRequests = Stream.of(this.allUserRequests).filter((ur) ->
                    (((ur.getStartDate() != null && ur.getStartDate().compareTo(yearStart) <= 0) && (ur.getStartDate() != null && ur.getStartDate().compareTo(yearEnd) >= 0)) ||
                    ((ur.getEndDate() != null && ur.getEndDate().compareTo(yearStart) >= 0) && (ur.getEndDate() != null && ur.getEndDate().compareTo(yearEnd) <= 0))) &&
                    UserRequests.RequestResults.approved.equals(ur.getApproveResult()) && user.getId().equals(ur.getRequestById())
                ).toArray(UserRequests[]::new);

                declinedProcessedRequests = Stream.of(this.allUserRequests).filter((ur) ->
                    (((ur.getStartDate() != null && ur.getStartDate().compareTo(yearStart) <= 0) && (ur.getStartDate() != null && ur.getStartDate().compareTo(yearEnd) >= 0)) ||
                    ((ur.getEndDate() != null && ur.getEndDate().compareTo(yearStart) >= 0) && (ur.getEndDate() != null && ur.getEndDate().compareTo(yearEnd) <= 0))) &&
                    UserRequests.RequestResults.declined.equals(ur.getApproveResult()) && user.getId().equals(ur.getRequestById())
                ).toArray(UserRequests[]::new);
                userRequests = ArrayUtils.addAll(approvedProcessedRequests, declinedProcessedRequests);
            }
        }
        catch (ParseException e) {
            e.printStackTrace();
        }

        return userRequests;
    }

    public static boolean canAutoApprove() {
        GroupManager groupManager = ComponentAccessor.getGroupManager();
        ApplicationUser currentUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();

        return groupManager.isUserInGroup(currentUser, VacationsHelper.beltechUserRoles.get("pm")) ||
               groupManager.isUserInGroup(currentUser, VacationsHelper.beltechUserRoles.get("ch")) ||
               groupManager.isUserInGroup(currentUser, VacationsHelper.beltechUserRoles.get("qa"));
    }

    public String requestToString(UserRequests request, String approvedBy)
    {
        SimpleDateFormat iso_date = new SimpleDateFormat("yyyy-MM-dd");
        String result = "";

        switch (request.getRequestType()) {
            case working_day_transfer:
            case day_off:
                result += "Day off";
                break;
            case cancel:
                result += "Cancel";
                break;
            case sick_day:
                result += "Sick day";
                break;
            case vacation:
                result += "Vacation";
                break;
        }

        if (request.getRequestType().equals(UserRequests.RequestTypes.cancel)) {
            UserRequests[] requestsToCancel = Stream.of(this.allUserRequests).filter((ur) -> ur.getID() == request.getCancelledUserRequestId()).toArray(UserRequests[]::new);

            if (requestsToCancel.length > 0) {
                UserRequests requestToCancel = requestsToCancel[0];
                if (requestToCancel != null) {
                    result += " '" + this.requestToString(requestToCancel, VacationsHelper.withoutApproveInf) + "'";
                }
            }
        }
        else {
            if (request.getRequestType().equals(UserRequests.RequestTypes.working_day_transfer)) {
                result += " on " + iso_date.format(request.getStartDate()) + ". I can work instead on " + iso_date.format(request.getEndDate());
            }
            else {
                if (request.getStartDate().equals(request.getEndDate())) {
                    result +=  " on " + iso_date.format(request.getStartDate());
                }
                else if ((request.getStartDate() != null) && (request.getEndDate() != null) && (request.getStartDate() != request.getEndDate())) {
                    result +=  " from " + iso_date.format(request.getStartDate()) + " to " + iso_date.format(request.getEndDate());
                }
            }

            if (!approvedBy.equals("without_approve_inf")) {
                if (request.getRequestById().equals(request.getApprovedById())) {
                    result +=  " (auto approved)";
                }
                else {
                    if (request.getApproveResult() != null) {
                        result +=  " was " + request.getApproveResult().toString();
                    }
                    if ((request.getApproveResult() != null) && (!approvedBy.isEmpty())) {
                        result +=  " by " + approvedBy;
                    }
                }
            }
        }

        if (request.getDescription() != null) {
            result +=  " (" + request.getDescription() + ")";
        }
        if (request.getPmDescription() != null) {
            result +=  " PM: " + request.getPmDescription();
        }

        return result;
    }
}
