package com.plansource.beltech.BeltechPlugin.TimesheetValidator.servlet;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.velocity.VelocityManager;
import com.google.common.collect.Maps;
import com.plansource.beltech.BeltechPlugin.GroupsManagement.GroupsManagementHelper;
import com.plansource.beltech.BeltechPlugin.TimesheetValidator.TimesheetValidatorHelper;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.Map;

@Scanned
public class TimesheetValidatorServlet extends HttpServlet
{
    private static final Logger log = LoggerFactory.getLogger(TimesheetValidatorServlet.class);

    private VelocityManager velocityManager;
    private ActiveObjects activeObjects;
    private WebResourceManager webResourceManager;
    private UserManager userManager;

    public TimesheetValidatorServlet(
        @ComponentImport VelocityManager velocityManager,
        @ComponentImport ActiveObjects activeObjects,
        @ComponentImport WebResourceManager webResourceManager,
        @ComponentImport UserManager userManager
    )
    {
        this.velocityManager = velocityManager;
        this.activeObjects = activeObjects;
        this.webResourceManager = webResourceManager;
        this.userManager = userManager;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException
    {
        ApplicationUser currentUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
        Map<String, Object> context = Maps.newHashMap();

        if (currentUser != null) {
            LocalDate from;
            LocalDate to;

            if (request.getParameter("start-date") == null || request.getParameter("start-date").isEmpty() || request.getParameter("end-date") == null || request.getParameter("end-date").isEmpty()) {
                LocalDate localDate = LocalDate.now();

                if (isWeekendDay(localDate)) {
                    from = localDate.with(TemporalAdjusters.previous(DayOfWeek.MONDAY));
                    to = localDate.with(TemporalAdjusters.nextOrSame(DayOfWeek.SUNDAY));
                }
                else {
                    LocalDate prevSaturday = localDate.with(TemporalAdjusters.previous(DayOfWeek.SATURDAY));
                    from = prevSaturday.with(TemporalAdjusters.previous(DayOfWeek.MONDAY));
                    to = localDate.with(TemporalAdjusters.previous(DayOfWeek.SUNDAY));
                }
            }
            else {
                String dateRangeStart = request.getParameter("start-date");
                String dateRangeEnd = request.getParameter("end-date");

                from = LocalDate.parse(dateRangeStart);
                to = LocalDate.parse(dateRangeEnd);
            }

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("YYYY-MM-dd");

            GroupsManagementHelper groupsManagementHelper = new GroupsManagementHelper(this.activeObjects, this.userManager);
            TimesheetValidatorHelper timesheetValidatorHelper = new TimesheetValidatorHelper(this.activeObjects, from, to);

            context.put("dateFrom", formatter.format(from));
            context.put("dateTo", formatter.format(to));
            context.put("groupsManagementHelper", groupsManagementHelper);
            context.put("timesheetValidatorHelper", timesheetValidatorHelper);
            context.put("unselectedDevelopers", groupsManagementHelper.unselectedDevelopers);
        }

        this.webResourceManager.requireResource("com.plansource.beltech.BeltechPlugin:jquery-ui-resources");
        this.webResourceManager.requireResource("com.plansource.beltech.BeltechPlugin:timesheet-validator-resources");
        String content = this.velocityManager.getEncodedBody("/templates/", "timesheet-validator.vm", "UTF-8", context);
        response.setContentType("text/html;charset=utf-8");
        response.getWriter().write(content);
        response.getWriter().close();
    }

    private boolean isWeekendDay(LocalDate date)
    {
        String[] weekend = {"SATURDAY", "SUNDAY"};
        int pos = StringUtils.indexOfAny(date.getDayOfWeek().name(), weekend);

        return pos != -1;
    }
}
