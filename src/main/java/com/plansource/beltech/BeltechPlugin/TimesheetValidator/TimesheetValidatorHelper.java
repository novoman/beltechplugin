package com.plansource.beltech.BeltechPlugin.TimesheetValidator;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.user.ApplicationUser;
import com.plansource.beltech.BeltechPlugin.Vacations.ao.UserRequests;
import com.plansource.beltech.BeltechPlugin.WorklogGenerator.servlet.DataGenerator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class TimesheetValidatorHelper {

    private ActiveObjects activeObjects;
    private ArrayList<HashMap<String, String>> timesheetData;
    private LocalDate dateFrom;
    private LocalDate dateTo;

    public TimesheetValidatorHelper(ActiveObjects activeObjects, LocalDate dateFrom, LocalDate dateTo)
    {
        this.activeObjects = activeObjects;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.timesheetData = getTimesheetData(dateFrom, dateTo);
    }

    public String checkTimesheetForUser(ApplicationUser user)
    {
        if (user == null) {
            return "";
        }

        String result = "";

        for (HashMap<String, String> userData : this.timesheetData) {
            if (user.getKey().equals(userData.get("user_key"))) {
                //check timesheet for date
                if (("true".equals(userData.get("incomplete")) || "-".equals(userData.get("hours"))) && !"true".equals(userData.get("vacation"))) {
                    result += "<div style=\"color: red;\">Working hours are not filled for " + userData.get("date") + "</div>";
                }
            }
        }

        result += checkWorkerDayTransfers(user);

        if (result.isEmpty()) {
            result = "Timesheet: <b><span style=\"color: green;\">OK</span></b>";
        }

        return result;
    }

    private String checkWorkerDayTransfers(ApplicationUser user)
    {
        if (user == null) {
            return "";
        }

        String result = "";
        boolean flag;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        Date startDate = Date.from(this.dateFrom.atStartOfDay(ZoneId.systemDefault()).toInstant());
        Date endDate = Date.from(this.dateTo.atStartOfDay(ZoneId.systemDefault()).toInstant());

        UserRequests[] userRequests = this.activeObjects.find(UserRequests.class, "(END_DATE >= ? AND END_DATE <= ?) AND REQUEST_TYPE = ? AND APPROVE_RESULT = ? AND REQUEST_BY_ID = ?", sdf.format(startDate), sdf.format(endDate), UserRequests.RequestTypes.working_day_transfer, UserRequests.RequestResults.approved, user.getId());
        if (userRequests.length > 0) {
            for (UserRequests userRequest : userRequests) {
                flag = false;
                for (HashMap<String, String> userData : this.timesheetData) {
                    if (user.getKey().equals(userData.get("user_key"))) {
                            SimpleDateFormat inSDF = new SimpleDateFormat("MM/dd/yyyy");
                            SimpleDateFormat outSDF = new SimpleDateFormat("yyyy-MM-dd");
                            String inDate = userData.get("date");
                            String outDate = "";
                            if (inDate != null) {
                                try {
                                    Date date = inSDF.parse(inDate);
                                    outDate = outSDF.format(date);

                                    if (outDate.equals(userRequest.getEndDate().toString().substring(0, 10)) && "false".equals(userData.get("incomplete"))) {
                                        flag = true;
                                    }
                                    if (flag) break;
                                } catch (ParseException ignored){}
                            }

                    }
                }

                if (!flag) {
                    sdf = new SimpleDateFormat("MM/dd/yyyy");
                    result += "<div style=\"color: red;\">Working day transfer hours are not filled for " + sdf.format(userRequest.getEndDate()) + "</div>";
                }
            }
        }

        return result;
    }

    private ArrayList<HashMap<String, String>> getTimesheetData(LocalDate dateFrom, LocalDate dateTo)
    {
        DataGenerator data = new DataGenerator(activeObjects);

        return data.generateDataForTimesheetValidator(dateFrom, dateTo);
    }
}
